/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.sync.ObjectSyncListener;
import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import ar.com.zir.skorpio.core.sync.SynchronizedObject;
import ar.com.zir.skorpio.core.ui.SkorpioUI;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Notification;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jmrunge
 */
public abstract class FormProvider {
    public abstract List<SkorpioListColumn> getFormListColumns();
    public abstract SkorpioBeanItemContainer getFormContainer();    
    public abstract void doRemoveItem(Object item, SystemUser user) throws PersistenceException;
    public abstract Class getObjectClass();    
    public abstract Page getTableObjects();
    public abstract List<SkorpioAbmField> getFormFields();    
    public abstract Object getNewObject();
    public abstract Object doCreateObject(Object item, SystemUser user) throws PersistenceException;
    public abstract Object doUpdateObject(Object item, SystemUser user) throws PersistenceException;
    public abstract SkorpioPersistedForm getFormInstance(String action);
    public abstract Object getFreshObject(Object item) throws PersistenceException;

    protected void addListFormAsListener(SkorpioListForm form, Class objClass, ObjectChangedListener listener) {
        ObjectSyncListener syncListener = new ObjectSyncListener() {
            @Override
            public Class getObjectClass() {
                return objClass;
            }

            @Override
            public void objectLocked(SynchronizedObject obj) {}

            @Override
            public void objectReleased(SynchronizedObject obj) {
                listener.objectChanged(form, obj.getObject());
            }

            @Override
            public void objectRemoved(SynchronizedObject obj) {}

            @Override
            public void objectAdded(SynchronizedObject obj) {}
        };
        form.addWindowClosedListener((Object item) -> {
            SkorpioUI.getCurrent().removeListener(syncListener);
        });
        SkorpioUI.getCurrent().addListener(syncListener);
    }
    
    protected void addAbmFormAsListener(SkorpioAbmForm form, Class objClass, ObjectChangedListener changeListener, ObjectAddedListener addListener, ObjectRemovedListener removeListener) {
        ObjectSyncListener syncListener = new ObjectSyncListener() {
            @Override
            public Class getObjectClass() {
                return objClass;
            }

            @Override
            public void objectLocked(SynchronizedObject obj) {}

            @Override
            public void objectReleased(SynchronizedObject obj) {
                changeListener.objectChanged(form, obj.getObject());
            }

            @Override
            public void objectRemoved(SynchronizedObject obj) {
                removeListener.objectRemoved(form, obj.getObject());
            }

            @Override
            public void objectAdded(SynchronizedObject obj) {
                addListener.objectAdded(form, obj.getObject());
            }
        };
        form.addWindowClosedListener((Object item) -> {
            SkorpioUI.getCurrent().removeListener(syncListener);
        });
        SkorpioUI.getCurrent().addListener(syncListener);
    }
    
    protected interface ObjectChangedListener {
        public void objectChanged(SkorpioPersistedForm form, Object obj);
    }
    
    protected interface ObjectAddedListener {
        public void objectAdded(SkorpioPersistedForm form, Object obj);
    }
    
    protected interface ObjectRemovedListener {
        public void objectRemoved(SkorpioPersistedForm form, Object obj);
    }
    
    protected SynchronizableObject refreshObject(String property, SynchronizableObject object) throws PersistenceException {
        return object;
    }
    
    protected Object getObjectProperty(Object obj, String property) {
        String methodName = "get" + property.substring(0, 1).toUpperCase() + property.substring(1);
        try {
            Method method = obj.getClass().getMethod(methodName, (Class<?>[]) null);
            Object value = method.invoke(obj, (Object[]) null);
            return value;
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    protected void addOrUpdateTableFieldItem(SkorpioAbmForm form, SynchronizableObject item, String childrenProperty, String parentProperty) {
        SynchronizableObject formObject = (SynchronizableObject) form.getFormObject();
        if (formObject.getId() != null) {
            try {
                item = refreshObject(childrenProperty, item);
                SynchronizableObject parent = (SynchronizableObject) getObjectProperty(item, parentProperty);
                if (parent != null) {
                    if (parent.equals(formObject)) {
                        boolean exists = false;
                        Collection children = (Collection) getObjectProperty(formObject, childrenProperty);
                        if (children != null) {
                            for (Object obj : children) {
                                if (obj.equals(item)) {
                                    exists = true;
                                    break;
                                }
                            }
                        }
                        if (exists) 
                            form.updateContainerItem(childrenProperty, item);
                        else
                            form.addContainerItem(childrenProperty, item);
                        if (children != null) {
                            children.remove(item);
                            children.add(item);
                        }
                    } else {
                        form.removeContainerItem(childrenProperty, item);
                        Collection children = (Collection) getObjectProperty(formObject, childrenProperty);
                        if (children != null) 
                            children.remove(item);
                    }
                    form.updateFormBeanItem(new BeanItem<>(formObject));    
                }
            } catch (PersistenceException ex) {
                Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
                Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
            }
        }
    }
     
    protected void removeTableFieldItem(SkorpioAbmForm form, SynchronizableObject item, String childrenProperty, String parentProperty) {
        SynchronizableObject formObject = (SynchronizableObject) form.getFormObject();
        if (formObject.getId() != null) {
            Object parent = getObjectProperty(item, parentProperty);
            if (parent != null) {
                if (parent.equals(formObject)) {
                    form.removeContainerItem(childrenProperty, item);
                    Collection children = (Collection) getObjectProperty(formObject, childrenProperty);
                    if (children != null) {
                        children.remove(item);
                        form.updateFormBeanItem(new BeanItem<>(formObject));
                    }
                }
            } else {
                form.removeContainerItem(childrenProperty, item);
                Collection children = (Collection) getObjectProperty(formObject, childrenProperty);
                if (children != null) {
                    children.remove(item);
                    form.updateFormBeanItem(new BeanItem<>(formObject));
                }
            }
        }
    }
    
    protected void updateComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String property) {
        try {
            form.updateContainerItem(property, refreshObject(property, item));
        } catch (PersistenceException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
        }
    }
    
    protected void addComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String property) {
        try {
            form.addContainerItem(property, refreshObject(property, item));
        } catch (PersistenceException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
        }
    }
    
    protected void removeComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String property) {
        form.removeContainerItem(property, item);
    }
    
    protected void updateChildren(SkorpioListForm form, SynchronizableObject parent, String parentProperty, String childrenProperty) {
        try {
            parent = refreshObject(parentProperty, parent);
            if (parentProperty.contains(".")) {
                String[] parentProps = parentProperty.split("\\.");
                String[] childrenProps = childrenProperty.split("\\.");
                String childProp = childrenProps[parentProps.length - 1];
                Collection children = (Collection) getObjectProperty(parent, childProp);
                for (Object child : children) {
                    updateChildren(form, (SynchronizableObject) child, parentProperty.substring(0, parentProperty.lastIndexOf(".")), childrenProperty.substring(0, childrenProperty.lastIndexOf(".")));
                }
            } else {
                Collection children = (Collection) getObjectProperty(parent, childrenProperty);
                for (Object child : children) {
                    form.updateItem((SynchronizableObject) child);
                }
            }
            
            
        } catch (PersistenceException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing " + parent.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
        }
    }
    
    protected void updateChildComboFieldItem() {
        //si es provincia
            //si agrega
                //la agrega a provincia container
                //actualiza el pais en el container de paises
            //si elimina
                //la elimina de provincia container
                //actualiza el pais en el container de paises
                //si estaba seleccionada
                    //setea null el combo de provincias
            //si modifica
                //la actualiza en el container de provincia
                //actualiza todos los paises en el container de paises
                //si estaba seleccionada
                    //setea el pais correspondiente en el combo de paises
        //si es localidad
            //si agrega
                //la agrega a localidad container
                //actualiza la provincia en el container de provincias
                //actualiza el pais en el container de paises
            //si elimina
                //la elimina de localidad container
                //actualiza la provincia en el container de provincias
                //actualiza el pais en el container de paises
                //si estaba seleccionada
                    //setea null el combo de localidades
            //si modifica
                //la actualiza en el container de localidades
                //actualiza todas las provincias en el container de provincias
                //actualiza todos los paises en el container de paises
                //si estaba seleccionada
                    //setea el pais correspondiente en el combo de paises
                    //setea la provincia correspondiente en el combo de provincias
    }
    
    protected void updateChildComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String childContainerProperty, String[] parentContainerProperties, String[] parentProperties) {
        try {
            SynchronizableObject selected = (SynchronizableObject) form.getFieldValue(childContainerProperty);
            boolean isSelected = selected.equals(item);
            item = refreshObject(childContainerProperty, item);
            form.updateContainerItem(childContainerProperty, item);
            Map<String, SynchronizableObject> values = new HashMap<>();
            values.put(childContainerProperty, item);
            for (int i = 0; i < parentContainerProperties.length; i++) {
                Collection parents = form.getContainerValues(parentContainerProperties[i]);
                for (Object parent : parents) {
                    parent = refreshObject(parentContainerProperties[i], (SynchronizableObject) parent);
                    form.updateContainerItem(parentContainerProperties[i], (SynchronizableObject) parent);
                }
                SynchronizableObject parent = (SynchronizableObject) getObjectProperty(item, parentProperties[i]);
                values.put(parentContainerProperties[i], parent);
                item = parent;
            }
            if (isSelected) {
                for (int i = parentContainerProperties.length - 1; i >= 0 ; i--) {
                    SynchronizableObject value = values.get(parentContainerProperties[i]);
                    form.setFieldValue(parentContainerProperties[i], value);
                }
                SynchronizableObject value = values.get(childContainerProperty);
                form.setFieldValue(childContainerProperty, value);
            }
        } catch (PersistenceException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
        }
    }
    
    protected void removeChildComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String childContainerProperty, String[] parentContainerProperties, String[] parentProperties) {
        try {
            SynchronizableObject selected = (SynchronizableObject) form.getFieldValue(childContainerProperty);
            boolean isSelected = selected.equals(item);
            form.removeContainerItem(childContainerProperty, item);
            for (int i = 0; i < parentContainerProperties.length; i++) {
                SynchronizableObject parent = (SynchronizableObject)getObjectProperty(item, parentProperties[i]);
                parent = refreshObject(parentContainerProperties[i], parent);
                form.updateContainerItem(parentContainerProperties[i], parent);
                item = parent;
            }
            if (isSelected)
                form.setFieldValue(childContainerProperty, null);
        } catch (PersistenceException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
        }
    }    
    
    protected void addChildComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String childContainerProperty, String[] parentContainerProperties, String[] parentProperties) {
        try {
            item = refreshObject(childContainerProperty, item);
            form.addContainerItem(childContainerProperty, item);
            for (int i = 0; i < parentContainerProperties.length; i++) {
                SynchronizableObject parent = (SynchronizableObject)getObjectProperty(item, parentProperties[i]);
                parent = refreshObject(parentContainerProperties[i], parent);
                form.updateContainerItem(parentContainerProperties[i], parent);
                item = parent;
            }
        } catch (PersistenceException ex) {
            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
        }
    }
    
//    protected void addOrUpdateChildComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String childContainerProperty, String parentProperty, String parentContainerProperty, String fullHierarchy, String objectName) {
//        try {
//            item = refreshObject(childContainerProperty, item);
//            SynchronizableObject formParent = (SynchronizableObject) form.getFieldValue(parentContainerProperty);
//            SynchronizableObject itemParent = (SynchronizableObject) getObjectProperty(item, parentProperty);
//            if (formParent.equals(itemParent)) {
//                Collection items = form.getContainerValues(childContainerProperty);
//                boolean exists = false;
//                for (Object obj : items) {
//                    if (obj.equals(item)) {
//                        exists = true;
//                        break;
//                    }
//                }
//                if (exists)
//                    form.updateContainerItem(childContainerProperty, item);
//                else
//                    form.addContainerItem(childContainerProperty, item);
//            } else {
//                form.removeContainerItem(childContainerProperty, item);
//            }
//            updateParentContainers(form, item, fullHierarchy, objectName);
//        } catch (PersistenceException ex) {
//            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
//            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
//        }
//    }
    
//    protected void removeChildComboFieldItem(SkorpioAbmForm form, SynchronizableObject item, String property, String fullHierarchy, String objectName) {
//        form.removeContainerItem(property, item);
//        try {
//            updateParentContainers(form, item, fullHierarchy, objectName);
//        } catch (PersistenceException ex) {
//            Logger.getLogger(FormProvider.class.getName()).log(Level.SEVERE, null, ex);
//            Notification.show("Error", "Error refreshing " + item.getClass().getSimpleName(), Notification.Type.ERROR_MESSAGE);
//        }
//    }
    
//    protected void updateParentContainers(SkorpioAbmForm form, SynchronizableObject item, String fullHierarchy, String objectName) throws PersistenceException {
//        String[] objects = fullHierarchy.split("\\.");
//        int lastIndex = objects.length - 1;
//        String containerProp = objects[0];
//        boolean process = false;
//        for (int i = 0; i < lastIndex; i++) {
//            String objProp = containerProp;
//            Object selected = form.getFieldValue(objProp);
//            containerProp = containerProp + "." + objects[i + 1];
//            if (!process) {
//                if (objects[i].equals(objectName))
//                    process = true;
//            }
//            if (process) {
//                Collection items = form.getContainerValues(containerProp);
//                for (Object obj : items) {
//                    obj = refreshObject(containerProp, (SynchronizableObject)obj);
//                    form.updateContainerItem(containerProp, (SynchronizableObject)obj);
//                }
//                if (selected != null && selected.equals(item)) {
//                    SynchronizableObject parent = (SynchronizableObject) getObjectProperty(item, objects[i + 1]);
//                    form.setFieldValue(containerProp, parent);
//                    form.setFieldValue(objProp, item);
//                }
//            }
//        }
//    }
}
