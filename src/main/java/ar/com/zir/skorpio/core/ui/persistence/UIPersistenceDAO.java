/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.persistence;

import ar.com.zir.skorpio.core.options.SystemOption;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jmrunge
 */
public class UIPersistenceDAO {
    private SystemOption selectedOption;
    private List<OpenedWindow> openedWindows;

    public SystemOption getSelectedOption() {
        return selectedOption;
    }

    public void setSelectedOption(SystemOption selectedOption) {
        this.selectedOption = selectedOption;
    }

    public List<OpenedWindow> getOpenedWindows() {
        return openedWindows;
    }

    public void setOpenedWindows(List<OpenedWindow> openedWindows) {
        this.openedWindows = openedWindows;
    }
    
    public void addWindow(OpenedWindow window) {
        if (openedWindows == null) 
            openedWindows = new ArrayList<>();
        openedWindows.add(window);
    }
    
    public void addWindow(SystemOption option, String instanceId, String action, Object item, boolean editing) {
        addWindow(new OpenedWindow(option, instanceId, action, item, editing));
    }
    
    public static final class OpenedWindow {
        private SystemOption option;
        private String action;
        private String instanceId;
        private Object item;
        private boolean editing;

        public OpenedWindow(SystemOption option, String instanceId, String action, Object item, boolean editing) {
            this.option = option;
            this.item = item;
            this.action = action;
            this.instanceId = instanceId;
            this.editing = editing;
        }

        public SystemOption getSystemOption() {
            return option;
        }

        public void setSystemOption(SystemOption option) {
            this.option = option;
        }

        public Object getItem() {
            return item;
        }

        public void setItem(Object item) {
            this.item = item;
        }

        public String getInstanceId() {
            return instanceId;
        }

        public void setInstanceId(String instanceId) {
            this.instanceId = instanceId;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }

        public boolean isEditing() {
            return editing;
        }

        public void setEditing(boolean editing) {
            this.editing = editing;
        }

    }
}
