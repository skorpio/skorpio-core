/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.persistence;

import ar.com.zir.skorpio.core.options.SystemOption;
import ar.com.zir.skorpio.core.security.SystemUser;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.inject.Singleton;

/**
 *
 * @author jmrunge
 */
@LocalBean
@Singleton
public class SkorpioUIPersistenceService extends UIPersistenceService {
    private Map<SystemUser, UIPersistenceDAO> sessions;
    
    protected Map<SystemUser, UIPersistenceDAO> getSessions() {
        if (sessions == null)
            sessions = new HashMap<>();
        return sessions;
    }
    
    protected void addOrUpdateSession(SystemUser su, UIPersistenceDAO session) {
        getSessions().put(su, session);
    }
    
    @Override
    public void addWindow(SystemUser su, SystemOption option, String instanceId, String action, Object item, boolean editing) {
        addWindow(su, new UIPersistenceDAO.OpenedWindow(option, instanceId, action, item, editing));
    }
    
    @Override
    public void setSelectedOption(SystemUser su, SystemOption so) {
        UIPersistenceDAO session = getSavedSession(su);
        session.setSelectedOption(so);
        saveSession(su, session);
    }

    @Override
    public void addWindow(SystemUser su, UIPersistenceDAO.OpenedWindow window) {
        UIPersistenceDAO session = getSavedSession(su);
        if (session == null) 
            session = new UIPersistenceDAO();
        session.addWindow(window);
        saveSession(su, session);
    }

    @Override
    public void saveSession(SystemUser su, UIPersistenceDAO session) {
        addOrUpdateSession(su, session);
    }

    @Override
    public List<UIPersistenceDAO.OpenedWindow> getWindows(SystemUser su) {
        UIPersistenceDAO session = getSavedSession(su);
        if (session != null)
            return session.getOpenedWindows();
        else
            return new ArrayList<>();
    }

    @Override
    public UIPersistenceDAO getSavedSession(SystemUser su) {
        UIPersistenceDAO session = getSessions().get(su);
        if (session == null)
            session = new UIPersistenceDAO();
        return session;
    }

    @Override
    public void removeWindow(SystemUser su, String windowInstance) {
        UIPersistenceDAO session = getSavedSession(su);
        UIPersistenceDAO.OpenedWindow toRemove = null;
        for (UIPersistenceDAO.OpenedWindow window : session.getOpenedWindows()) {
            if (window.getInstanceId().equals(windowInstance)) {
                toRemove = window;
                break;
            }
        }
        if (toRemove != null)
            session.getOpenedWindows().remove(toRemove);
    }

    @Override
    public void clearSession(SystemUser su) {
        getSessions().remove(su);
    }

    @Override
    public void updateWindow(SystemUser su, SystemOption option, String instanceId, String action, Object item, boolean editing) {
        UIPersistenceDAO session = getSavedSession(su);
        if (session != null) {
            session.getOpenedWindows().stream().filter((window) -> (window.getSystemOption().equals(option) && window.getInstanceId().equals(instanceId))).forEach((window) -> {
                window.setAction(action);
                window.setItem(item);
                window.setEditing(editing);
            });
            saveSession(su, session);
        }
    }

    @Override
    public void updateWindow(SystemUser su, UIPersistenceDAO.OpenedWindow window) {
        updateWindow(su, window.getSystemOption(), window.getInstanceId(), window.getAction(), window.getItem(), window.isEditing());
    }
    
}
