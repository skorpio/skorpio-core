/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.main;

import ar.com.zir.skorpio.core.options.SystemOption;
import org.vaadin.addon.cdimvp.MVPView;

/**
 *
 * @author jmrunge
 */
public interface SkorpioMainView extends MVPView {
    public void showMenu(SystemOption option);
    public void showHome();
    public void addWindow(SystemOption option, String action);
    public void addWindow(SystemOption option, String action, Object item, boolean editing);
}
