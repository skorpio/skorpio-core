package ar.com.zir.skorpio.core.ui;


import ar.com.zir.skorpio.core.config.SystemConfig;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.security.SecurityService;
import ar.com.zir.skorpio.core.sync.LoggedUsersListener;
import ar.com.zir.skorpio.core.sync.SkorpioSyncService;
import ar.com.zir.skorpio.core.sync.ObjectSyncListener;
import ar.com.zir.skorpio.core.sync.SyncListener;
import ar.com.zir.skorpio.core.sync.SynchronizedObject;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import javax.servlet.http.Cookie;
import org.vaadin.addon.cdimvp.AbstractMVPView;

public abstract class SkorpioUI extends UI implements SyncListener, LoggedUsersListener {
    private SystemUser user;
    private String id;
    private Map<Class, List<ObjectSyncListener>> listeners  = new HashMap<>();
    private AbstractMVPView currentView;
    
    @Inject
    private Instance<SkorpioView> mainView;
    @Inject
    private SystemConfig config;
    @Inject
    private SecurityService securityService;
    @Inject
    private SkorpioSyncService syncService;
    
    @Override
    protected void init(final VaadinRequest request) {
        VaadinSession.getCurrent().setConverterFactory(new SkorpioConverterFactory());
        currentView = (AbstractMVPView) mainView.get();
        setContent(currentView);
        getPage().setTitle(config.getAppName());
        currentView.enter();
        id = UUID.randomUUID().toString();
        System.out.println("My ID: " + id);
        syncService.addSyncListener(this);
    }
    
    @Override
    public void userLoggedIn(SystemUser su) {
        if (user != null && user.equals(su)) {
//            access(this::logout);
            if (this.getSession() != null && this.getSession().getSession() != null)
                this.getSession().getSession().setMaxInactiveInterval(1);
        }
    }
    
    @Override
    public void userLoggedOut(SystemUser su){}
    
    public boolean login(SystemUser su) {
        su = securityService.login(su);
        if (su != null) {
            this.user = su;
            saveCurrentUser();
            this.getSession().getSession().setMaxInactiveInterval(config.getIdleTime());
            syncService.removeLoggedUsersListener(this);
            syncService.login(user);
            syncService.addLoggedUsersListener(this);
            return true;
        } else {
            return false;
        }
    }
    
    public void logout() {
        this.getSession().getSession().invalidate();
//        this.close();
        URI uri = this.getPage().getLocation();
        System.out.println("Openening main page: " + uri.toString());
        this.getPage().setLocation(uri);
    }
    
    @Override
    public void close() {
        syncService.logout(user);
        this.user = null;
//        System.out.println("Removing sync listener");
//        syncService.removeSyncListener(this);
//        System.out.println("Removing login listener");
//        syncService.removeLoggedUsersListener(this);
        System.out.println("Calling super.close");
        super.close();
        System.out.println("Returned from super.close");
        if (this.getSession() != null) {
            System.out.println("Trying to invalidate http session");
            if (this.getSession().getSession() != null) {
                System.out.println("Invalidating vaadin session");
                try {
                    this.getSession().getSession().invalidate(); 
                } catch (Exception ex) {
                    Logger.getLogger(SkorpioUI.class.getName()).log(Level.SEVERE, "Error invalidating session", ex);
                }
            }
            System.out.println("Trying with vaadin service");
            if (VaadinService.getCurrentRequest() != null) {
                if (VaadinService.getCurrentRequest().getWrappedSession(false) != null) {
                    System.out.println("Doing it with VaadinService");
                    try {
                        VaadinService.getCurrentRequest().getWrappedSession(false).invalidate();
                    } catch (Exception ex) {
                        Logger.getLogger(SkorpioUI.class.getName()).log(Level.SEVERE, "Error invalidating vaadin service session", ex);
                    }
                }
                VaadinService.reinitializeSession(VaadinService.getCurrentRequest());
            }
            System.out.println("Closing vaadin session");
            try {
                this.getSession().close();
//                PARA PROBAR
//                VaadinService.getCurrent().closeSession(this.getSession());
            } catch (Exception ex) {
                Logger.getLogger(SkorpioUI.class.getName()).log(Level.SEVERE, "Error closing vaadin session", ex);
            }
        }
        URI uri = this.getPage().getLocation();
        System.out.println("Openening main page: " + uri.toString());
        this.getPage().setLocation(uri);
    }
    
    private void saveCurrentUser() {
        Cookie cookie = getCookieByName("user");
        if (cookie != null) {
            cookie.setValue(user.getStringRepresentation());
        } else {
            cookie = new Cookie("user", user.getStringRepresentation());
        }
        cookie.setMaxAge(60*60*24*365);
        cookie.setPath(VaadinService.getCurrentRequest().getContextPath());
        VaadinService.getCurrentResponse().addCookie(cookie);
    }
    
    public SystemUser getSavedUser() {
        Cookie cookie = getCookieByName("user");
        if (cookie != null) {
            SystemUser su = securityService.getNewSystemUser();
            su.parseStringRepresentation(cookie.getValue());
            return su;
        } else {
            return null;
        }
    }
    
    private Cookie getCookieByName(String name) {
        Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();
        for (Cookie cookie : cookies) {
            if (cookie.getName().equals(name))
                return cookie;
        }
        return null;
    }
    
    public SystemUser getUser() {
        return user;
    }
    
    public static SkorpioUI getCurrent() {
        return (SkorpioUI) UI.getCurrent();
    }
    
//    @Override
//    public Future access(Runnable run) {
//        System.out.println("UI ID: " + id);
//        return super.access(run);
//    }
    
    @Override
    public void detach() {
        System.out.println("Removing sync listener");
        syncService.removeSyncListener(this);
        System.out.println("Removing login listener");
        syncService.removeLoggedUsersListener(this);
        System.out.println("Calling super.detach");
        super.detach();
    }
    
    public synchronized void addListener(ObjectSyncListener listener) {
        if (!listeners.containsKey(listener.getObjectClass())) 
            listeners.put(listener.getObjectClass(), new ArrayList<>());
        
        List<ObjectSyncListener> lst = listeners.get(listener.getObjectClass());
        lst.add(listener);
        listeners.put(listener.getObjectClass(), lst);
    }
    
    public synchronized void removeListener(ObjectSyncListener listener) {
        List<ObjectSyncListener> lst = listeners.get(listener.getObjectClass());
        if (lst != null) {
            lst.remove(listener);
            if (lst.isEmpty())
                listeners.remove(listener.getObjectClass());
            else
                listeners.put(listener.getObjectClass(), lst);
        }
    }

    @Override
    public void objectLocked(SynchronizedObject obj) {
        List<ObjectSyncListener> lst = listeners.get(obj.getObjectClass());
        if (lst != null) {
            access(() -> {
                lst.stream().forEach((listener) -> {
                    listener.objectLocked(obj);
                });
            });
        }
    }

    @Override
    public void objectReleased(SynchronizedObject obj) {
        List<ObjectSyncListener> lst = listeners.get(obj.getObjectClass());
        if (lst != null) {
            access(() -> {
                lst.stream().forEach((listener) -> {
                    listener.objectReleased(obj);
                });
            });
        }
    }

    @Override
    public void objectRemoved(SynchronizedObject obj) {
        List<ObjectSyncListener> lst = listeners.get(obj.getObjectClass());
        if (lst != null) {
            access(() -> {
                lst.stream().forEach((listener) -> {
                    listener.objectRemoved(obj);
                });
            });
        }
    }

    @Override
    public void objectAdded(SynchronizedObject obj) {
        List<ObjectSyncListener> lst = listeners.get(obj.getObjectClass());
        if (lst != null) {
            access(() -> {
                lst.stream().forEach((listener) -> {
                    listener.objectAdded(obj);
                });
            });
        }
    }
    
}
