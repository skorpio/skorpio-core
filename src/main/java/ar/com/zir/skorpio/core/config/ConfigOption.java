/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.config;

import ar.com.zir.skorpio.core.persistence.InternalEntity;
import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.TableGenerator;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
@NamedQueries({
    @NamedQuery(name = "ConfigOption.findByCodigo", query = "select c from ConfigOption c where c.codigo = :codigo"),
    @NamedQuery(name = "ConfigOption.findAllVisible", query = "select c from ConfigOption c where c.hidden = false"),
    @NamedQuery(name = "ConfigOption.findAll", query = "select c from ConfigOption c")
})
@Entity
@TableGenerator(allocationSize = 1, 
        name = "ConfigOptionGenerator", 
        pkColumnValue = "ConfigOption")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class ConfigOption<T> implements Serializable, SynchronizableObject, InternalEntity {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "ConfigOptionGenerator")
    private Integer id;
    @NotNull
    @Size(max = 40)
    private String codigo;
    @NotNull
    @Size(max = 80)
    private String descripcion;
    @NotNull
    private Boolean internal;
    @NotNull
    private Boolean hidden;

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getHidden() {
        return hidden;
    }

    public void setHidden(Boolean hidden) {
        this.hidden = hidden;
    }

    @Override
    public Boolean getInternal() {
        return internal;
    }

    @Override
    public void setInternal(Boolean internal) {
        this.internal = internal;
    }
    
    public abstract T getValor();
    public abstract void setValor(T value);

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigOption)) {
            return false;
        }
        ConfigOption other = (ConfigOption) object;
        return !((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id)));
    }

    @Override
    public String toString() {
        return codigo;
    }
    
    @Override
    public Class getMainClass() {
        return ConfigOption.class;
    }
}
