/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.options;

import java.util.List;

/**
 *
 * @author jmrunge
 */
public abstract class SystemOptions {
    public static final String ACTION_LIST = "list";
    public static final String ACTION_ADD = "add";
    public static final String ACTION_VIEW = "view";
    public static final String ACTION_EDIT = "edit";
    public static final String ACTION_REMOVE = "remove";

    public abstract List<SystemOption> getRootSystemOptions();
    public abstract List<SystemOption> getSecuritySystemOptions();
}
