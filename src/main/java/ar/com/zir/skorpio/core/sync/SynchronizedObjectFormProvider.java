/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.sync;

import ar.com.zir.skorpio.core.config.DbSystemConfig;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.forms.FormProvider;
import ar.com.zir.skorpio.core.ui.forms.SkorpioAbmField;
import ar.com.zir.skorpio.core.ui.forms.SkorpioBeanItemContainer;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListColumn;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListForm;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import com.vaadin.ui.Notification;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author jmrunge
 */
@Singleton
@LocalBean
public class SynchronizedObjectFormProvider extends FormProvider {
    @Inject
    private SkorpioSyncService syncService;
    @Inject
    private DbSystemConfig config;

    @Override
    public List<SkorpioListColumn> getFormListColumns() {
        return syncService.getLockedObjectColumns();    
    }

    @Override
    public SkorpioBeanItemContainer getFormContainer() {
        SkorpioBeanItemContainer container = new SkorpioBeanItemContainer(SynchronizedObject.class);
        container.addNestedContainerProperty("systemUser.userName");
        container.addNestedContainerProperty("objectClass.simpleName");
        return container;
    }

    @Override
    public void doRemoveItem(Object item, SystemUser user) throws PersistenceException {
        SynchronizedObject obj = (SynchronizedObject) item;
        try {
            syncService.forceObjectRelease(obj.getObject(), obj.getSystemUser());
        } catch (SyncException ex) {
            Logger.getLogger(SynchronizedObjectFormProvider.class.getName()).log(Level.SEVERE, null, ex);
            Notification.show("Error releasing object", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    @Override
    public Class getObjectClass() {
        return SynchronizedObject.class;
    }

    @Override
    public Page getTableObjects() {
        return syncService.getPagedLockedObjects(config.getConfigIntegerValue("default.cant.rows"));
    }

    @Override
    public List<SkorpioAbmField> getFormFields() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getNewObject() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object doCreateObject(Object item, SystemUser user) throws PersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object doUpdateObject(Object item, SystemUser user) throws PersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public SkorpioPersistedForm getFormInstance(String action) {
        switch (action) {
            case SystemOptions.ACTION_LIST:
                return new SkorpioListForm();
//            case MilongaOptions.ACTION_ADD:
//            case MilongaOptions.ACTION_EDIT:
//                return new SystemRoleForm();
            default:
                return null;
        }
    }

    @Override
    public Object getFreshObject(Object item) throws PersistenceException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
