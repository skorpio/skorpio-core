/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

import com.vaadin.data.util.BeanItemContainer;

/**
 *
 * @author jmrunge
 */
public class SkorpioBeanItemContainer extends BeanItemContainer {

    public SkorpioBeanItemContainer(Class type) throws IllegalArgumentException {
        super(type);
    }
    
    public void refreshData() {
        fireItemSetChange();
    }
}
