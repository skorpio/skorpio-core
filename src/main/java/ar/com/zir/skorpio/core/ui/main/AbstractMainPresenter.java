/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.main;

import ar.com.zir.skorpio.core.options.SystemOption;
import javax.enterprise.event.Observes;
import org.vaadin.addon.cdimvp.AbstractMVPPresenter;
import org.vaadin.addon.cdimvp.CDIEvent;
import org.vaadin.addon.cdimvp.ParameterDTO;

/**
 *
 * @author jmrunge
 */
@SuppressWarnings("serial")
public abstract class AbstractMainPresenter extends AbstractMVPPresenter<SkorpioMainView> {
    
    public static final String SHOW_MENU = "show_menu";
    public static final String SHOW_HOME = "show_home";
    public static final String ADD_WINDOW = "add_window";

    protected void show_menu(@Observes @CDIEvent(SHOW_MENU) final ParameterDTO parameters) {
        view.showMenu((SystemOption)parameters.getPrimaryParameter());
    }

    protected void show_home(@Observes @CDIEvent(SHOW_HOME) final ParameterDTO parameters) {
        view.showHome();
    }
    
    protected void add_window(@Observes @CDIEvent(ADD_WINDOW) final ParameterDTO parameters) {
        view.addWindow((SystemOption)parameters.getPrimaryParameter(), (String)parameters.getSecondaryParameters()[0], parameters.getSecondaryParameters().length > 2 ? parameters.getSecondaryParameters()[1] : null, parameters.getSecondaryParameters().length > 2 ? (Boolean)parameters.getSecondaryParameters()[2] : false);
    }

    @Override
    public void viewEntered() {
        // NOP
    }
    
}
