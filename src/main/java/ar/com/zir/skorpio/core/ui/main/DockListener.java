/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.main;

import ar.com.zir.skorpio.core.ui.SkorpioDockItem;

/**
 *
 * @author jmrunge
 */
public interface DockListener {
    public void buttonClicked(SkorpioDockItem item);
}
