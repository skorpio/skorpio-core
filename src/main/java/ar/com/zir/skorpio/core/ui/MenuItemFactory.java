/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.options.SystemOption;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import org.vaadin.sebastian.dock.DockItem;

/**
 *
 * @author jmrunge
 */
@LocalBean
@Stateless
public class MenuItemFactory {
    
    public MenuItemFactory() {
    }
    
    public DockItem getDockItem(SystemOption option) {
        return new SkorpioDockItem(option);
    }
    
    public SkorpioMenuItem getMenuItem(SystemOption option, String action) {
        return new SkorpioMenuItem(option, action);
    }
    
}
