/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.sync;

import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.SynchronizedObjectsList;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListColumn;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.inject.Singleton;

/**
 *
 * @author jmrunge
 */
@Singleton
@LocalBean
public class SkorpioSyncService {
    private final Map<Class, List<SynchronizedObject>> lockedObjects = new HashMap<>();
    private final List<SyncListener> syncListeners = new ArrayList<>();
    private final List<LoggedUsersListener> usersListeners = new ArrayList<>();
    private final List<SystemUser> loggedUsers = new ArrayList<>();
    private static final String LOCK_EVENT = "lock";
    private static final String RELEASE_EVENT = "release";
    private static final String ADD_EVENT = "add";
    private static final String DELETE_EVENT = "delete";
    
    public synchronized void lockObject(SynchronizableObject obj, SystemUser user, String owner) throws SyncException {
        lockObject(obj, user, owner, null);
    }
    
    private void lockObject(SynchronizableObject obj, SystemUser user, String owner, SynchronizedObject parent) throws SyncException {
        List<SynchronizedObject> objects = lockedObjects.get(obj.getMainClass());
        if (objects != null) {
            for (SynchronizedObject o : objects) {
                if (o.getObjectId().equals(obj.getId()))
                    throw new SyncException("Object already locked by " + o.getSystemUser().toString());
            }
        }
        SynchronizedObject locked = new SynchronizedObject(obj, user, owner);
        if (parent == null) {
            parent = locked;
            locked.addRelatedObject(obj);
        }
        lockRelated(locked, parent);
        if (parent.equals(locked))
            locked.getRelatedObjects().remove(obj);
        fireObjectEvent(locked, LOCK_EVENT);
        objects = lockedObjects.get(obj.getMainClass());
        if (objects == null) 
            objects = new ArrayList<>();
        objects.add(locked);
        lockedObjects.put(obj.getMainClass(), objects);
        fireObjectEvent(new SynchronizedObject(locked, user, ""), ADD_EVENT);
    }
    
    private void lockRelated(SynchronizedObject obj, SynchronizedObject parent) throws SyncException {
        for (Method m : obj.getObject().getClass().getMethods()) {
            for (Annotation a : m.getAnnotationsByType(Related.class)) {
                try {
                    Object o = m.invoke(obj.getObject(), (Object[]) null);
                    if (o instanceof Collection) {
                        for (Object related : (Collection)o) {
//                            parent.addRelatedObject((SynchronizableObject) related);
//                            lockObject((SynchronizableObject)related, obj.getSystemUser(), obj.getOwner(), parent);
                            lockRelatedObject((SynchronizableObject) related, parent);
                        }
                    } else {
//                        parent.addRelatedObject((SynchronizableObject)o);
//                        lockObject((SynchronizableObject)o, obj.getSystemUser(), obj.getOwner(), parent);
                        lockRelatedObject((SynchronizableObject) o, parent);
                    }
                } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                    Logger.getLogger(SkorpioSyncService.class.getName()).log(Level.SEVERE, null, ex);
                    throw new SyncException("Error locking related object");
                }
            }
        }
    }
    
    private void lockRelatedObject(SynchronizableObject obj, SynchronizedObject parent) throws SyncException {
        boolean locked = false;
        for (SynchronizableObject child : parent.getRelatedObjects()) {
            if (child.equals(obj)) {
                locked = true;
                break;
            }
        }
        if (!locked) {
            parent.addRelatedObject((SynchronizableObject)obj);
            lockObject((SynchronizableObject)obj, parent.getSystemUser(), parent.getOwner(), parent);
        }
    }
    
    private void fireObjectEvent(SynchronizedObject obj, String event) {
        System.out.println("Firing " + event + " for " + obj.getObjectClass().getSimpleName() + ", " + obj.getObjectId());
        syncListeners.stream()
                .filter(listener -> (listener instanceof SyncListener || ((listener instanceof ObjectSyncListener) && ((ObjectSyncListener)listener).getObjectClass().equals(obj.getObjectClass()))))
                .forEach(listener -> {
            switch (event) {
                case LOCK_EVENT:
                    listener.objectLocked(obj);
                    break;
                case RELEASE_EVENT:
                    listener.objectReleased(obj);
                    break;
                case ADD_EVENT:
                    listener.objectAdded(obj);
                    break;
                case DELETE_EVENT:
                    listener.objectRemoved(obj);
                    break;
            }
        });
        lockedObjects.keySet().stream().forEach((c) -> {
            lockedObjects.get(c).stream().forEach((o) -> {
                System.out.println("Still locked " + o.getObjectClass().getSimpleName() + ", " + o.getObjectId());
            });
        });
    }
    
    public synchronized void addSyncListener(SyncListener listener) {
        syncListeners.add(listener);
    }
    
    public synchronized void removeSyncListener(SyncListener listener) {
        syncListeners.remove(listener);
    }
    
    public synchronized void forceObjectRelease(SynchronizableObject obj, SystemUser user) throws SyncException {
        _releaseObject(obj, user, true, true);
    }
    
    public synchronized void releaseObject(SynchronizableObject obj, SystemUser user) throws SyncException {
        _releaseObject(obj, user, false, true);
    }
    
    private synchronized void _releaseObject(SynchronizableObject obj, SystemUser user, boolean forced, boolean checkRelated) throws SyncException {
        SynchronizedObject locked = getLockedObject(obj);
        
        if (locked == null)
            throw new SyncException("Object was not locked");
        else if (!locked.getSystemUser().equals(user) && !forced)
            throw new SyncException("Object was locked by " + user.toString());
        else if (forced)
            locked.setForcedReleaseUser(user);
        
        if (checkRelated) {
            releaseRelated(locked, user, forced);
            SynchronizedObject sync = new SynchronizedObject(obj, user, locked.getOwner());
            sync.addRelatedObject(obj);
            lockRelated(sync, sync);
            sync.getRelatedObjects().remove(obj);
            releaseRelated(sync, user, forced);
        }
            
        List<SynchronizedObject> objects = lockedObjects.get(obj.getMainClass());
        objects.remove(locked);
        lockedObjects.put(obj.getMainClass(), objects);
        fireObjectEvent(locked, RELEASE_EVENT);
        fireObjectEvent(new SynchronizedObject(locked, user, ""), DELETE_EVENT);
    }
    
    private void releaseRelated(SynchronizedObject obj, SystemUser user, boolean forced) throws SyncException {
        for (SynchronizableObject o : obj.getRelatedObjects()) {
            _releaseObject(o, user, forced, false);
        }
    }
    
    public synchronized SynchronizedObject getLockedObject(SynchronizableObject obj) {
        List<SynchronizedObject> objects = lockedObjects.get(obj.getMainClass());
        if (objects != null) {
            for (SynchronizedObject o : objects) {
                if (o.getObjectId().equals(obj.getId())) 
                    return o;
            }
        }
        return null;
    }
    
    public synchronized List<SynchronizedObject> getLockedObjects(Class clazz) {
        return lockedObjects.get(clazz);
    }
    
    public synchronized List<SynchronizedObject> getLockedObjects() {
        List<SynchronizedObject> objects = new ArrayList<>();
        lockedObjects.keySet().stream().forEach((key) -> {
            objects.addAll(getLockedObjects(key));
        });
        return objects;
    }
    
    public synchronized Page getPagedLockedObjects(int pageSize) {
        List<SynchronizedObject> objects = new ArrayList<>();
        lockedObjects.keySet().stream().forEach((key) -> {
            objects.addAll(getLockedObjects(key));
        });
        return new SynchronizedObjectsList(objects, pageSize);
    }
    
    public synchronized void updateLockedObjectOwner(SynchronizableObject obj, SystemUser user, String owner) throws SyncException {
        SynchronizedObject locked = getLockedObject(obj);
        if (locked != null) {
            updateRelatedLockedObjectsOwner(locked, user, owner);
            if (user.equals(locked.getSystemUser()))
                locked.setOwner(owner);
            else
                throw new SyncException("Object was locked by " + locked.getSystemUser().toString());
        } else {
            throw new SyncException("Object was not locked");
        }
    }
    
    private void updateRelatedLockedObjectsOwner(SynchronizedObject obj, SystemUser user, String owner) throws SyncException {
        for (SynchronizableObject o : obj.getRelatedObjects()) {
            updateLockedObjectOwner(o, user, owner);
        }
    }
    
    public synchronized void addObject(SynchronizableObject obj, SystemUser user, String owner) {
        fireObjectEvent(new SynchronizedObject(obj, user, owner), ADD_EVENT);
    }
    
    public synchronized void removeObject(SynchronizableObject obj, SystemUser user, String owner) throws SyncException {
        SynchronizedObject locked = getLockedObject(obj);
        if (locked != null)
            throw new SyncException("Object locked by " + locked.getSystemUser().toString());
        SynchronizedObject removed = new SynchronizedObject(obj, user, owner);
        fireObjectEvent(removed, DELETE_EVENT);
    }
    
    public List<SkorpioListColumn> getLockedObjectColumns() {
        List<SkorpioListColumn> columns = new ArrayList<>();
        columns.add(new SkorpioListColumn("systemUser.userName", String.class, "Usuario"));
        columns.add(new SkorpioListColumn("objectClass.simpleName", String.class, "Tipo de Objeto"));
        columns.add(new SkorpioListColumn("objectId", Integer.class, "Identificador"));
        return columns;
    }
    
    public synchronized void addLoggedUsersListener(LoggedUsersListener listener) {
        usersListeners.add(listener);
    }
    
    public synchronized void removeLoggedUsersListener(LoggedUsersListener listener) {
        usersListeners.remove(listener);
    }
    
    public synchronized void login(SystemUser user) {
        loggedUsers.add(user);
        fireSystemUserLogin(user);
    }
    
    private void fireSystemUserLogin(SystemUser user) {
        fireSystemUserEvent(user, true);
    }
    
    private void fireSystemUserEvent(SystemUser user, boolean login) {
        List<LoggedUsersListener> tmp = new ArrayList<>();
        usersListeners.stream().forEach((listener) -> {
            tmp.add(listener);
        });
        tmp.stream().forEach((listener) -> {
            if (login)
                listener.userLoggedIn(user);
            else
                listener.userLoggedOut(user);
        });
        
    }
    
    public synchronized void logout(SystemUser user) {
        loggedUsers.remove(user);
        fireSystemUserLogout(user);
    }
    
    private void fireSystemUserLogout(SystemUser user) {
        fireSystemUserEvent(user, false);
    }
    
    
}
