/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.config;

import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.forms.FormProvider;
import ar.com.zir.skorpio.core.ui.forms.SkorpioAbmField;
import ar.com.zir.skorpio.core.ui.forms.SkorpioAbmForm;
import ar.com.zir.skorpio.core.ui.forms.SkorpioBeanItemContainer;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListColumn;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListForm;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.LocalBean;
import javax.inject.Inject;
import javax.inject.Singleton;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
@Singleton
@LocalBean
public class ConfigParamsFormProvider extends FormProvider {
    @Inject
    private SystemConfig config;
 
    @Override
    public List<SkorpioListColumn> getFormListColumns() {
        List<SkorpioListColumn> columns = new ArrayList<>();
        columns.add(new SkorpioListColumn("codigo", String.class, "Código"));
        columns.add(new SkorpioListColumn("descripcion", String.class, "Descripción"));
        columns.add(new SkorpioListColumn("valor", Object.class, "Valor"));
        return columns;
    }

    @Override
    public SkorpioBeanItemContainer getFormContainer() {
        return new SkorpioBeanItemContainer(ConfigOption.class);
    }

    @Override
    public void doRemoveItem(Object item, SystemUser user) throws PersistenceException {
        throw new PersistenceException("Config options can't be removed");
    }

    @Override
    public Class getObjectClass() {
        return ConfigOption.class;
    }

    @Override
    public Page getTableObjects() {
        return config.getPagedConfigOptions();
    }

    @Override
    public List<SkorpioAbmField> getFormFields() {
        List<SkorpioAbmField> fields = new ArrayList<>();
        fields.add(new SkorpioAbmField("Código", "codigo", true, true, false));
        fields.add(new SkorpioAbmField("Descripción", "descripcion", true, true, false));
        fields.add(new SkorpioAbmField("Valor", "valor", true));
        return fields;
    }

    @Override
    public Object getNewObject() {
        return null;
    }

    @Override
    public Object doCreateObject(Object item, SystemUser user) throws PersistenceException {
        throw new PersistenceException("Config options can't be created");
    }

    @Override
    public Object doUpdateObject(Object item, SystemUser user) throws PersistenceException {
        return config.updateConfigOption((ConfigOption)item);
    }

    @Override
    public SkorpioPersistedForm getFormInstance(String action) {
        switch (action) {
            case SystemOptions.ACTION_LIST:
                SkorpioListForm listForm = new SkorpioListForm();
                return listForm;
            case SystemOptions.ACTION_EDIT:
                SkorpioAbmForm abmForm = new SkorpioAbmForm();
                return abmForm;
            default:
                return null;
        }
    }

    @Override
    public Object getFreshObject(Object item) throws PersistenceException {
        return config.refreshConfigOption((ConfigOption)item);
    }
    
}
