/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.sync;

import ar.com.zir.skorpio.core.security.SystemUser;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;

/**
 *
 * @author jmrunge
 */
public class SynchronizedObject implements SynchronizableObject {
    private SynchronizableObject object;
    private SystemUser user;
    private String owner;
    private SystemUser forcedReleaseUser;
    private Collection<SynchronizableObject> relatedObjects;
    
    public SynchronizedObject(SynchronizableObject obj, SystemUser user, String owner) {
        this.object = obj;
        this.user = user;
        this.owner = owner;
        this.relatedObjects = new ArrayList<>();
    }
    
    public SynchronizableObject getObject() {
        return object;
    }
    
    public SystemUser getSystemUser() {
        return user;
    }
    
    public Integer getObjectId() {
        return object.getId();
    }
    
    public Class getObjectClass() {
        return object.getMainClass();
    }
    
    public String getOwner() {
        return owner;
    }
    
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public SystemUser getForcedReleaseUser() {
        return forcedReleaseUser;
    }

    public void setForcedReleaseUser(SystemUser forcedReleaseUser) {
        this.forcedReleaseUser = forcedReleaseUser;
    }

    @Override
    public Integer getId() {
        return object.hashCode();
    }
    
    public Collection<SynchronizableObject> getRelatedObjects() {
        return relatedObjects;
    }
    
    public void addRelatedObject(SynchronizableObject obj) {
        relatedObjects.add(obj);
    }
    
    public void addRelatedObjects(Collection<SynchronizableObject> objects) {
        relatedObjects.addAll(objects);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 31 * hash + Objects.hashCode(this.object.getId());
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SynchronizedObject other = (SynchronizedObject) obj;
        return Objects.equals(this.object, other.object);
    }

    @Override
    public Class getMainClass() {
        return getClass();
    }

    @Override
    public void setId(Integer id) {}
    
}
