/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.persistence;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author jmrunge
 */
public class Page {
    private List results;
    private int pageSize;
    private int page;
    private int maxPages;
    private int totalSize;
    private String namedQuery;
    private EntityManager em;
    private Map<String, Object> params;
    
    public Page() {
        
    }

    public Page(int page, int pageSize) {
        //DO NOT USE!! JUST FOR EXTENDING IT EASYLY
        this(null, null, page, pageSize);
    }
   
    public Page(EntityManager em, String namedQuery, int page, int pageSize) {
        this(em, namedQuery, page, pageSize, null);
    }

    public Page(EntityManager em, String namedQuery, int page, int pageSize, Map<String, Object> params) {
        this(em, namedQuery, page, pageSize, 0, params);
    }

    public Page(EntityManager em, String namedQuery, int page, int pageSize, int totalSize) {
        this(em, namedQuery, page, pageSize, totalSize, null);
    }

    public Page(EntityManager em, String namedQuery, int page, int pageSize, int totalSize, Map<String, Object> params) {
        this.page = page;
        this.totalSize = totalSize;
        this.em = em;
        this.namedQuery = namedQuery;
        this.params = params;
        setPageSize(pageSize);
    }
    
    public void setPageSize(int pageSize) {
        int firstResult = page * pageSize;
        this.pageSize = pageSize;
        if (totalSize > 0) {
            this.maxPages = totalSize / pageSize;
            if ((pageSize * maxPages) < totalSize)
                maxPages++;
        } else {
            maxPages = 0;
        }
        setResults(firstResult);
    }
    
    public void gotoPage(int page) {
        this.page = page;
        setResults();
    }
    
    public void goLast() {
        if (maxPages > 0) {
            gotoPage(maxPages - 1);
            while(next()){};
        }
    }
    
    public void goFirst() {
        gotoPage(0);
    }
    
    protected void setResults() {
        setResults(0);
    }
    
    protected int getPage() {
        return page;
    }
    
    protected int getPageSize() {
        return pageSize;
    }
   
    protected void setResults(int firstResult) {
        if (firstResult == 0) 
            firstResult = page * pageSize;
                    
        Query query = em.createNamedQuery(namedQuery);
        if (params != null) {
            for (String key : params.keySet()) {
                query = query.setParameter(key, params.get(key));
            }
        }
        _setResults(query.setFirstResult(firstResult)
            .setMaxResults(pageSize+1)
            .getResultList());
    }
    
    protected void _setResults(List results) {
        this.results = results;
    }
    
    public boolean next() {
        if (isNextPage()) {
            page++;
            setResults();
            return true;
        } else {
            return false;
        }
    }
    
    public boolean previous() {
        if (isPreviousPage()) {
            page--;
            setResults();
            return true;
        } else {
            return false;
        }
    }
   
    public boolean isNextPage() {
        return results.size() > pageSize;
    }
   
    public boolean isPreviousPage() {
        return page > 0;
    }
   
    public List getList() {
        return isNextPage() ?
            results.subList(0, pageSize) :
            results;
    }
}
