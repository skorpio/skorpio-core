/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.initialconfig;

import ar.com.zir.skorpio.core.config.DbSystemConfig;
import ar.com.zir.skorpio.core.ui.AbstractSkorpioForm;
import ar.com.zir.skorpio.core.ui.AbstractSkorpioPresenter;
import com.vaadin.data.util.BeanItem;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.vaadin.addon.cdimvp.AbstractMVPView;

/**
 *
 * @author jmrunge
 */
public abstract class AbstractInitialConfigView extends AbstractMVPView implements InitialConfigView {
    @Inject 
    private DbSystemConfig config;
    
    @PostConstruct
    protected void initView() {
        setSizeFull();

        AbstractSkorpioForm form = getInitialConfigForm();
        form.addCommitListener((BeanItem bean) -> {
            if (config.saveInitialConfig(bean.getBean()))
                fireViewEvent(AbstractSkorpioPresenter.SHOW_LOGIN, null);
        });
        setCompositionRoot(form);
        form.setSizeFull();
    }
    
    protected abstract AbstractSkorpioForm getInitialConfigForm();
    
    protected abstract Object getInitialConfigBean();

    @Override
    public void enter() {
        super.enter();
        getInitialConfigForm().setBean(new BeanItem<>(getInitialConfigBean()));        
    }
}
