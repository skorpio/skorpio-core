/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.sync;

import ar.com.zir.skorpio.core.security.SystemUser;

/**
 *
 * @author jmrunge
 */
public interface LoggedUsersListener {
    public void userLoggedIn(SystemUser user);
    public void userLoggedOut(SystemUser user);
}
