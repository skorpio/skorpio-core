/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import com.vaadin.data.Validator.EmptyValueException;
import com.vaadin.data.Validator.InvalidValueException;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author jmrunge
 */
public abstract class AbstractSkorpioForm extends SkorpioPersistedForm {
    private BeanFieldGroup binder;
    private final List<CommitListener> listeners = new ArrayList<>();
    
    public void setBean(BeanItem bean) {
        binder = new BeanFieldGroup(bean.getBean().getClass());
        binder.setItemDataSource(bean);
        bindBean(binder);
        fixNullRepresentation();
    }
    
    protected BeanItem getBeanItem() {
        if (binder != null)
            return binder.getItemDataSource();
        else
            return null;
    }
    
    protected BeanFieldGroup getBinder() {
        return binder;
    }
    
    protected abstract void bindBean(FieldGroup binder);
    
    protected abstract void fixNullRepresentation();
    
    public void addCommitListener(CommitListener listener) {
        listeners.add(listener);
    }
    
    protected void commit() {
        try {
            binder.getFields().stream().forEach((field) -> {
                try {
                    field.validate();
                } catch (EmptyValueException ex) {
                    Notification.show("El campo " + field.getCaption() + " no puede estar vacío", Notification.Type.ERROR_MESSAGE);
                    throw ex;
                } catch (InvalidValueException ex) {
                    String message = "";
                    if (ex.getCauses() != null && ex.getCauses().length > 0)
                        message = ex.getCauses()[0].getMessage();
                    else
                        message = ex.getMessage();
                    if (message == null || message.trim().isEmpty())
                        message = "Error validando el campo " + field.getCaption();
                    Notification.show(message, Notification.Type.ERROR_MESSAGE);
                    throw ex;
                }
            });
            binder.commit();
            listeners.stream().forEach((listener) -> {
                listener.commit((BeanItem) binder.getItemDataSource());
            });
        } catch (InvalidValueException ex) {
        } catch (FieldGroup.CommitException ex) {
            Logger.getLogger(this.getClass().getCanonicalName()).log(Level.SEVERE, "Error realizando commit", ex);
            Notification.show("Error inesperado", Notification.Type.ERROR_MESSAGE);
        }
    }
    
    protected Component encapsulateComponent(Component comp) {
        FormLayout form = new FormLayout();
        form.setMargin(false);
        form.setSpacing(false);
        form.addComponent(comp);
        return form;
    }
    
    public interface CommitListener {
        public void commit(BeanItem bean);
    }
}
