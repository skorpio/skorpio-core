/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.options.SystemOption;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

/**
 *
 * @author jmrunge
 */
public class SkorpioMenuItem extends VerticalLayout {
    private SystemOption option;
    private String action;
    
    public SkorpioMenuItem(SystemOption option, String action) {
        super();
        this.option = option;
        this.action = action;
        Label lbl = new Label();
        lbl.setImmediate(false);
        lbl.setContentMode(ContentMode.HTML);
        lbl.setValue("<span style='cursor:pointer;'>" + option.getIcon().getHtml() + " " + option.getName() + "</span>");
        this.addComponent(lbl);
    }
    
    public SystemOption getSystemOption() {
        return option;
    }
    
    public String getAction() {
        return action;
    }
    
}
