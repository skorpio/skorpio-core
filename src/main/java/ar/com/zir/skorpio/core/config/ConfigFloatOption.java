/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.config;

import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import javax.persistence.Entity;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
@Entity
public class ConfigFloatOption extends ConfigOption<Float> implements SynchronizableObject {
    private static final long serialVersionUID = 1L;
    
    @NotNull
    private Float valor;

    @Override
    public Float getValor() {
        return valor;
    }

    @Override
    public void setValor(Float valor) {
        this.valor = valor;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ConfigFloatOption)) {
            return false;
        }
        ConfigFloatOption other = (ConfigFloatOption) object;
        return !((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId())));
    }

}
