/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

import java.util.List;

/**
 *
 * @author jmrunge
 */
public class SkorpioAbmTableField extends SkorpioAbmField {
    private List<SkorpioListColumn> columns;

    public SkorpioAbmTableField(String caption, String field, boolean required, List<SkorpioListColumn> columns) {
        this(caption, field, required, columns, true, null);
    }

    public SkorpioAbmTableField(String caption, String field, boolean required, List<SkorpioListColumn> columns, String[] styles) {
        this(caption, field, required, columns, true, styles);
    }

    public SkorpioAbmTableField(String caption, String field, boolean required, List<SkorpioListColumn> columns, boolean bind) {
        this(caption, field, required, columns, bind, null);
    }

    public SkorpioAbmTableField(String caption, String field, boolean required, List<SkorpioListColumn> columns, boolean bind, String[] styles) {
        super(caption, field, required, bind, styles);
        this.columns = columns;
    }

    public List<SkorpioListColumn> getColumns() {
        return columns;
    }

    public void setColumns(List<SkorpioListColumn> columns) {
        this.columns = columns;
    }
    
}
