/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.security;

/**
 *
 * @author jmrunge
 */
public interface SystemUser {
    public String getUserName();
    public String getPassword();
    public String getStringRepresentation();
    public void parseStringRepresentation(String representation);
//    @Size(min=4, max=10)
//    private String userName;
//    @Size(min=6, max=20)
//    private String password;
//
//    public SystemUser() {
//    }
//
//    public SystemUser(String userName, String password) {
//        this.userName = userName;
//        this.password = password;
//    }
//
//    public String getUserName() {
//        return userName;
//    }
//
//    public void setUserName(String userName) {
//        this.userName = userName;
//    }
//
//    public String getPassword() {
//        return password;
//    }
//
//    public void setPassword(String password) {
//        this.password = password;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 7;
//        hash = 71 * hash + Objects.hashCode(this.userName);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object obj) {
//        if (obj == null) {
//            return false;
//        }
//        if (getClass() != obj.getClass()) {
//            return false;
//        }
//        final SystemUser other = (SystemUser) obj;
//        if (!Objects.equals(this.userName, other.userName)) {
//            return false;
//        }
//        if (!Objects.equals(this.password, other.password)) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return userName;
//    }
//
//    public String getStringRepresentation() {
//        String representation = getUserName();
//        return representation;
//    }
//    
//    public void parseStringRepresentation(String representation) {
//        if (representation != null) {
//                setUserName(representation);
//        }
//    }
    
}
