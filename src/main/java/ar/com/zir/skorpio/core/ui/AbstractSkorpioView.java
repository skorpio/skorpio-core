/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.ui.main.AbstractMainPresenter;
import ar.com.zir.skorpio.core.config.SystemConfig;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.persistence.UIPersistenceDAO;
import ar.com.zir.skorpio.core.ui.persistence.UIPersistenceService;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import org.vaadin.addon.cdimvp.AbstractMVPView;
import org.vaadin.addon.cdimvp.MVPView;
import org.vaadin.dialogs.ConfirmDialog;

/**
 *
 * @author jmrunge
 */
@SuppressWarnings("serial")
public abstract class AbstractSkorpioView extends AbstractMVPView implements SkorpioView {
    private HorizontalLayout viewsLayout;
    private AbsoluteLayout topLayout;
    private String id;
    @Inject
    private Instance<MVPView> views;
    @Inject
    private SystemConfig config;
    @Inject
    private UIPersistenceService uiPersistence;
    @Inject
    private MenuItemFactory factory;

    @PostConstruct
    protected void initView() {
        if (id == null)
            id = UUID.randomUUID().toString();
        System.out.println("SkorpioView ID " + id);
        setSizeFull();

        final VerticalLayout mainLayout = new VerticalLayout();
        
        float height = SkorpioUI.getCurrent().getPage().getBrowserWindowHeight();
        float headerHeight = config.getHeaderHeight();

        if (headerHeight > 0) {
            topLayout = new AbsoluteLayout();
            topLayout.setSizeFull();
            if (config.getHeaderStyle().trim().length() != 0)
                topLayout.addStyleName(config.getHeaderStyle());
            mainLayout.addComponent(topLayout);
        }
        
        viewsLayout = new HorizontalLayout();
        viewsLayout.setSizeFull();
        viewsLayout.addStyleName(config.getMainStyle());
        mainLayout.addComponent(viewsLayout);
        
        if (headerHeight > 0) {
            float percent = (headerHeight / height) * 100f;
            mainLayout.setExpandRatio(topLayout, percent);
            mainLayout.setExpandRatio(viewsLayout, 100 - percent);
        }
        setCompositionRoot(mainLayout);
        mainLayout.setSizeFull();
    }

    @Override
    public void setMainView(final Class<? extends MVPView> viewClass) {
        viewsLayout.removeAllComponents();
        MVPView view = views.select(viewClass).get();
        viewsLayout.addComponent((Component)view);
        ((AbstractMVPView) view).enter();
    }
    
    @Override
    public void enter() {
        super.enter();
        fireViewEvent(AbstractSkorpioPresenter.SHOW_LOGIN, null);
    }

    @Override
    public void login(SystemUser user) {
        if (SkorpioUI.getCurrent().login(user)) {
            final SystemUser su = SkorpioUI.getCurrent().getUser();
            fireViewEvent(AbstractSkorpioPresenter.SHOW_MAIN, null);
            fireViewEvent(AbstractSkorpioPresenter.SHOW_USER_DATA, true);
            UIPersistenceDAO session = uiPersistence.getSavedSession(su);
            if (session != null) {
                if (session.getSelectedOption() != null) {
                    SkorpioDockItem item = (SkorpioDockItem) factory.getDockItem(session.getSelectedOption());
                    fireViewEvent(item.getSystemOption().getEvent(), item.getSystemOption());
                }
                if (session.getOpenedWindows() != null) {
                    List<UIPersistenceDAO.OpenedWindow> toAdd = new ArrayList<>();
                    List<String> toRemove = new ArrayList<>();
                    session.getOpenedWindows().stream().forEach((window) -> {
                        toAdd.add(window);
                        toRemove.add(window.getInstanceId());
                    });
                    toAdd.stream().forEach((window) -> {
                        fireViewEvent(AbstractMainPresenter.ADD_WINDOW, window.getSystemOption(), window.getAction(), window.getItem(), window.isEditing());
                    });
                    toRemove.stream().forEach((id) -> {
                        uiPersistence.removeWindow(su, id);
                    });
                }
            }
        } else {
            Notification.show("Usuario o password inválido", Notification.Type.ERROR_MESSAGE);
        }
    }

    @Override
    public void logout() {
        ConfirmDialog d = ConfirmDialog.show(SkorpioUI.getCurrent(), "Cerrar Sesión", "¿Desea cerrar la sesión?", "Sí", "No", (ConfirmDialog cd) -> {
            if (cd.isConfirmed()) {
                SkorpioUI.getCurrent().logout();
                fireViewEvent(AbstractSkorpioPresenter.SHOW_USER_DATA, false);
                fireViewEvent(AbstractSkorpioPresenter.SHOW_LOGIN, null);
            }
        });
        d.setWidth("15em");
        d.setHeight("9em");
    }
    
    protected AbsoluteLayout getTopLayout() {
        return topLayout;
    }
    
}
