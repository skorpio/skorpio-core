/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.persistence;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public interface InternalEntity {
    public void setInternal(Boolean internal);
    public Boolean getInternal();
}
