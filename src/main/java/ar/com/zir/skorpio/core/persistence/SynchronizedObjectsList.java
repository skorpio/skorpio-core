/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.persistence;

import java.util.List;

/**
 *
 * @author jmrunge
 */
public class SynchronizedObjectsList extends Page {
    private List objects;
    
    public SynchronizedObjectsList(List objects, int pageSize) {
        this.objects = objects;
        setPageSize(pageSize);
    }
    
    @Override
    protected void setResults(int firstResult) {
        if (firstResult == 0) 
            firstResult = getPage() * getPageSize();
                    
        int toIndex = firstResult + getPageSize() + 1;
        if (toIndex > objects.size())
                toIndex = objects.size();
        _setResults(objects.subList(firstResult, toIndex));
        
    }
}
