/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.security.SystemUser;
import org.vaadin.addon.cdimvp.MVPView;

/**
 *
 * @author jmrunge
 */
public interface SkorpioView extends MVPView {
    public void setMainView(final Class<? extends MVPView> viewClass);
    public void login(SystemUser su);
    public void logout();
    public void showUserData(boolean show);
}
