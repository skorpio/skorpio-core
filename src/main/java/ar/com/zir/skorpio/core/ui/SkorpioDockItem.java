/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.options.SystemOption;
import com.vaadin.server.ThemeResource;
import org.vaadin.sebastian.dock.DockItem;

/**
 *
 * @author jmrunge
 */
public class SkorpioDockItem extends DockItem {
    private SystemOption option;
    
    public SkorpioDockItem(SystemOption option) {
        super(new ThemeResource(option.getResource()), option.getName());
        this.option = option;
    }
    
    public SystemOption getSystemOption() {
        return option;
    }
    
}
