/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.session;

import com.vaadin.server.CustomizedSystemMessages;
import com.vaadin.server.SystemMessagesInfo;
import com.vaadin.server.VaadinServlet;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

/**
 *
 * @author jmrunge
 */
@WebServlet(value = "/*",
            asyncSupported = true)
public class SkorpioServlet extends VaadinServlet {

    @Override
    protected void servletInitialized() throws ServletException {
        super.servletInitialized();
        getService().setSystemMessagesProvider((SystemMessagesInfo systemMessagesInfo) -> {
            CustomizedSystemMessages messages = new CustomizedSystemMessages();
            messages.setSessionExpiredCaption("Finalizó la sesión");
            messages.setSessionExpiredMessage("Su sesión se ha cerrado.  Haga click <u>aquí</u> para ingresar de vuelta al sistema.");
            return messages;
        });
    }
    
}
