/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui;

import com.vaadin.data.util.converter.Converter;
import com.vaadin.data.util.converter.DefaultConverterFactory;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jmrunge
 */
public class SkorpioConverterFactory extends DefaultConverterFactory {
    private List<Converter> converters = new ArrayList<>();
    
    public SkorpioConverterFactory() {
        converters.add(new SetToListConverter());
    }
    
    public void addConverter(Converter converter) {
        converters.add(converter);
    }

    @Override
    public <PRESENTATION, MODEL> Converter<PRESENTATION, MODEL> findConverter(Class<PRESENTATION> presentationType, Class<MODEL> modelType) {
//        System.out.println("Busca converter " + presentationType.getSimpleName() + " a " + modelType.getSimpleName());
//        if (presentationType.equals(Set.class) && modelType.equals(List.class)) {
////            System.out.println("Entro a buscarla");
//            return new Converter() {
//
//                @Override
//                public Object convertToModel(Object value, Class targetType, Locale locale) throws Converter.ConversionException {
//                    if (value == null) return null;
//                    List list = new ArrayList();
//                    for (Object obj : (Set)value) {
//                        list.add(obj);
//                    }
////                    System.out.println("Convirtio a modelo");
//                    return list;
//                }
//
//                @Override
//                public Object convertToPresentation(Object value, Class targetType, Locale locale) throws Converter.ConversionException {
//                    if (value == null) return null;
//                    LinkedHashSet set = new LinkedHashSet();
//                    for (Object obj : (List)value) {
//                        set.add(obj);
//                    }
////                    System.out.println("Convirtio a presentation");
//                    return set;
//                }
//
//                @Override
//                public Class getModelType() {
//                    return List.class;
//                }
//
//                @Override
//                public Class getPresentationType() {
//                    return Set.class;
//                }
//                
//            };
//        }
        for (Converter converter : converters) {
            if (presentationType.equals(converter.getPresentationType()) && modelType.equals(converter.getModelType())) {
                return converter;
            }
        }
        
        return super.findConverter(presentationType, modelType);
    }
    
}
