/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.persistence;

import ar.com.zir.skorpio.core.options.SystemOption;
import ar.com.zir.skorpio.core.security.SystemUser;
import java.util.List;

/**
 *
 * @author jmrunge
 */
public abstract class UIPersistenceService {
    public abstract void addWindow(SystemUser su, SystemOption option, String instanceId, String action, Object item, boolean editing);
    public abstract void addWindow(SystemUser su, UIPersistenceDAO.OpenedWindow window);
    public abstract void updateWindow(SystemUser su, SystemOption option, String instanceId, String action, Object item, boolean editing);
    public abstract void updateWindow(SystemUser su, UIPersistenceDAO.OpenedWindow window);
    public abstract void removeWindow(SystemUser su, String windowInstance);
    public abstract void saveSession(SystemUser su, UIPersistenceDAO session);
    public abstract List<UIPersistenceDAO.OpenedWindow> getWindows(SystemUser su);
    public abstract UIPersistenceDAO getSavedSession(SystemUser su);
    public abstract void clearSession(SystemUser su);
    public abstract void setSelectedOption(SystemUser su, SystemOption so);
}
