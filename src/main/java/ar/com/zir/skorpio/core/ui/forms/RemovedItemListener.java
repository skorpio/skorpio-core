/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

import ar.com.zir.skorpio.core.sync.SyncException;

/**
 *
 * @author jmrunge
 */
public interface RemovedItemListener {
    public void fireItemRemoved(Object item) throws SyncException;
}
