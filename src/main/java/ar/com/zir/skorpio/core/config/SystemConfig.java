/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.config;

import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import java.util.List;

/**
 *
 * @author jmrunge
 */
public abstract class SystemConfig {
    public abstract String getAppName();
    public abstract String getHeaderStyle();
    public abstract String getMainStyle();
    public abstract float getHeaderHeight();
    public abstract int getIdleTime();
    public abstract List<ConfigOption> getConfigOptions();
    public abstract Page getPagedConfigOptions();
    public abstract ConfigOption updateConfigOption(ConfigOption option) throws PersistenceException;
    public abstract ConfigOption updateConfigOption(ConfigOption option, boolean force) throws PersistenceException;
    public abstract ConfigOption refreshConfigOption(ConfigOption option) throws PersistenceException;
}
