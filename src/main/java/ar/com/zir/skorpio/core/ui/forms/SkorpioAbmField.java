/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

/**
 *
 * @author jmrunge
 */
public class SkorpioAbmField {
    private String caption;
    private String field;
    private boolean required;
    private boolean bind;
    private boolean editable;
    private String[] styles;
    private FieldLocation location;
    private Class fieldClass;

    public SkorpioAbmField(String caption, String field, boolean required) {
        this(caption, field, required, true, null);
    }
    
    public SkorpioAbmField(String caption, String field, boolean required, String[] styles) {
        this(caption, field, required, true, styles);
    }

    public SkorpioAbmField(String caption, String field, boolean required, boolean bind) {
        this(caption, field, required, bind, true, null);
    }
    
    public SkorpioAbmField(String caption, String field, boolean required, boolean bind, String[] styles) {
        this(caption, field, required, bind, true, styles);
    }

    public SkorpioAbmField(String caption, String field, boolean required, boolean bind, boolean editable) {
        this(caption, field, required, bind, editable, null);
    }

    public SkorpioAbmField(String caption, String field, boolean required, boolean bind, boolean editable, String[] styles) {
        this.caption = caption;
        this.field = field;
        this.required = required;
        this.bind = bind;
        this.editable = editable;
        this.styles = styles;
        this.location = null;
        this.fieldClass = null;
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(boolean required) {
        this.required = required;
    }

    public boolean isBind() {
        return bind;
    }

    public void setBind(boolean bind) {
        this.bind = bind;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(boolean editable) {
        this.editable = editable;
    }

    public String[] getStyles() {
        return styles;
    }

    public void setStyles(String[] styles) {
        this.styles = styles;
    }

    public FieldLocation getLocation() {
        return location;
    }

    public void setLocation(FieldLocation location) {
        this.location = location;
    }

    public Class getFieldClass() {
        return fieldClass;
    }

    public void setFieldClass(Class fieldClass) {
        this.fieldClass = fieldClass;
    }
    
    public static class FieldLocation {
        int x;
        int y;
        int width;
        int height;
        
        public FieldLocation(int x, int y, int width, int height) {
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }

        public int getX() {
            return x;
        }

        public void setX(int x) {
            this.x = x;
        }

        public int getY() {
            return y;
        }

        public void setY(int y) {
            this.y = y;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
        
        
    }
}
