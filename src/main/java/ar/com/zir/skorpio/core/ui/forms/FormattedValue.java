/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

import java.text.ParseException;
import javax.swing.text.MaskFormatter;

/**
 *
 * @author jmrunge
 */
public class FormattedValue {
    private String format;
    private Object value;

    public FormattedValue(Object value, String format) {
        this.format = format;
        this.value = value;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
    
    public String getFormattedValue() throws ParseException {
        MaskFormatter mf = new MaskFormatter(format);
        mf.setValueContainsLiteralCharacters(false);
        return mf.valueToString(value);
    }
    
}
