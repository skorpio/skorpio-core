/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.forms;

/**
 *
 * @author jmrunge
 */
public class SkorpioListColumn {
    private String id;
    private Class clazz;
    private String title;

    public SkorpioListColumn(String id, Class clazz, String title) {
        this.id = id;
        this.clazz = clazz;
        this.title = title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Class getClazz() {
        return clazz;
    }

    public void setClazz(Class clazz) {
        this.clazz = clazz;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
