/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.login;

import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.security.SecurityService;
import ar.com.zir.skorpio.core.ui.AbstractSkorpioPresenter;
import ar.com.zir.skorpio.core.ui.AbstractSkorpioForm;
import ar.com.zir.skorpio.core.ui.SkorpioUI;
import com.vaadin.data.util.BeanItem;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.vaadin.addon.cdimvp.AbstractMVPView;

/**
 *
 * @author jmrunge
 */
public abstract class AbstractLoginView extends AbstractMVPView implements LoginView {
    @Inject
    private SecurityService securityService;
    
    @PostConstruct
    protected void initView() {
        setSizeFull();

        AbstractSkorpioForm form = getLoginForm();
        form.addCommitListener((BeanItem bean) -> {
            fireViewEvent(AbstractSkorpioPresenter.LOGIN, bean.getBean());
        });
        setCompositionRoot(form);
        form.setSizeFull();
    }
    
    public abstract AbstractSkorpioForm getLoginForm();

    @Override
    public void enter() {
        super.enter();
        SystemUser su = SkorpioUI.getCurrent().getSavedUser();
        if (su == null)
            su = securityService.getNewSystemUser();
        getLoginForm().setBean(new BeanItem<>(su));        
    }

}
