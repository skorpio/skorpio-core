/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.config;

import ar.com.zir.skorpio.core.persistence.Page;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import java.math.BigInteger;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public abstract class DbSystemConfig extends SystemConfig {

    @Override
    public String getAppName() {
        return getConfigStringValue("app.name");
    }

    @Override
    public String getHeaderStyle() {
        return getConfigStringValue("app.header.style");
    }

    @Override
    public String getMainStyle() {
        return getConfigStringValue("app.main.style");
    }

    @Override
    public float getHeaderHeight() {
        return getConfigFloatValue("app.header.height");
    }
    
    @Override
    public int getIdleTime() {
        return getConfigIntegerValue("app.idle.time");
    }
    
    public void addConfigStringOption(String codigo, String descripcion, String value, boolean internal, boolean hidden) throws PersistenceException {
        ConfigStringOption option = new ConfigStringOption();
        option.setValor(value);
        addConfigOption(codigo, descripcion, option, internal, hidden);
    }

    public void addConfigFloatOption(String codigo, String descripcion, Float value, boolean internal, boolean hidden) throws PersistenceException {
        ConfigFloatOption option = new ConfigFloatOption();
        option.setValor(value);
        addConfigOption(codigo, descripcion, option, internal, hidden);
    }
    
    public void addConfigIntegerOption(String codigo, String descripcion, Integer value, boolean internal, boolean hidden) throws PersistenceException {
        ConfigIntegerOption option = new ConfigIntegerOption();
        option.setValor(value);
        addConfigOption(codigo, descripcion, option, internal, hidden);
    }
    
    private void addConfigOption(String codigo, String descripcion, ConfigOption option, boolean internal, boolean hidden) throws PersistenceException {
        ConfigOption test = getConfigOption(codigo);
        if (test != null)
            throw new PersistenceException("Config option already exists: " + codigo);
        option.setCodigo(codigo);
        option.setDescripcion(descripcion);
        option.setInternal(internal);
        option.setHidden(hidden);
        getEntityManager().persist(option);
        getEntityManager().flush();
    }
    
    public String getConfigStringValue(String codigo) {
        ConfigStringOption value = (ConfigStringOption) getConfigOption(codigo);
        if (value != null)
            return ((ConfigStringOption)value).getValor();
        else
            return null;
    }
    
    public Integer getConfigIntegerValue(String codigo) {
        ConfigIntegerOption value = (ConfigIntegerOption) getConfigOption(codigo);
        if (value != null)
            return ((ConfigIntegerOption)value).getValor();
        else
            return null;
    }
    
    public Float getConfigFloatValue(String codigo) {
        ConfigFloatOption value = (ConfigFloatOption) getConfigOption(codigo);
        if (value != null)
            return ((ConfigFloatOption)value).getValor();
        else
            return null;
    }
    
    public ConfigOption getConfigOption(String codigo) {
        try {
            return (ConfigOption) getEntityManager().createNamedQuery("ConfigOption.findByCodigo").setParameter("codigo", codigo).getSingleResult();
        } catch (NoResultException ex){
            return null;
        }
    }
    
    @Override
    public List<ConfigOption> getConfigOptions() {
        return getEntityManager().createNamedQuery("ConfigOption.findAllVisible").getResultList();
    }
    
    @Override
    public Page getPagedConfigOptions() {
        BigInteger total = (BigInteger) getEntityManager().createNativeQuery("select count(*) from ConfigOption").getSingleResult();
        return new Page(getEntityManager(), "ConfigOption.findAllVisible", 0, getConfigIntegerValue("default.cant.rows"), total.intValue());
    }
    
    @Override
    public ConfigOption updateConfigOption(ConfigOption option) throws PersistenceException {
        return updateConfigOption(option, false);
    }

    @Override
    public ConfigOption updateConfigOption(ConfigOption option, boolean force) throws PersistenceException {
        if (option.getInternal() && !force)
            throw new PersistenceException("Internal options can't be modified");
        ConfigOption newOption = getEntityManager().merge(option);
        getEntityManager().flush();
        return newOption;
    }
    
    @Override
    public ConfigOption refreshConfigOption(ConfigOption option) throws PersistenceException {
        try {
            option = getEntityManager().find(ConfigOption.class, option.getId());
            getEntityManager().refresh(option);
            return option;
        } catch (Exception ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error refrescando la opción", ex);
            throw new PersistenceException("Error refrescando la opción", ex);
        }
    }

    protected abstract EntityManager getEntityManager();
    
    public abstract boolean saveInitialConfig(Object initialConfig);
    
    public abstract boolean initialConfigDone();
    
}
