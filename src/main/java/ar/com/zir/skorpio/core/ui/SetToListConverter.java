/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui;

import com.vaadin.data.util.converter.Converter;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

/**
 *
 * @author jmrunge
 */
public class SetToListConverter implements Converter {
    @Override
    public Object convertToModel(Object value, Class targetType, Locale locale) throws Converter.ConversionException {
        if (value == null) return null;
        List list = new ArrayList();
        for (Object obj : (Set)value) {
            list.add(obj);
        }
//                    System.out.println("Convirtio a modelo");
        return list;
    }

    @Override
    public Object convertToPresentation(Object value, Class targetType, Locale locale) throws Converter.ConversionException {
        if (value == null) return null;
        LinkedHashSet set = new LinkedHashSet();
        for (Object obj : (List)value) {
            set.add(obj);
        }
//                    System.out.println("Convirtio a presentation");
        return set;
    }

    @Override
    public Class getModelType() {
        return List.class;
    }

    @Override
    public Class getPresentationType() {
        return Set.class;
    }
}
