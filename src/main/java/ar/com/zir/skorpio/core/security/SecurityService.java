/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.security;

/**
 *
 * @author jmrunge
 */
public abstract class SecurityService {
    public abstract SystemUser login(SystemUser su);
    public abstract SystemUser getNewSystemUser();
}
