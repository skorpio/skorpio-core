/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.session;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 *
 * @author jmrunge
 */
public class HttpListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent hse) {
        System.out.println("Inicio la sesion");
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent hse) {
        System.out.println("Finalizo la sesion");
    }
    
}
