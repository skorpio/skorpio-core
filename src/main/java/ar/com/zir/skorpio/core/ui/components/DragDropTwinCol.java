/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.components;

import com.vaadin.data.Container;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.DataBoundTransferable;
import com.vaadin.event.dd.DragAndDropEvent;
import com.vaadin.event.dd.DropHandler;
import com.vaadin.event.dd.acceptcriteria.AcceptCriterion;
import com.vaadin.event.dd.acceptcriteria.And;
import com.vaadin.event.dd.acceptcriteria.ClientSideCriterion;
import com.vaadin.event.dd.acceptcriteria.SourceIs;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomField;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.ColumnHeaderMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 *
 * @author jmrunge
 */
public class DragDropTwinCol extends CustomField<Collection> {
    private String itemProperty;
    private BeanItemContainer sourceContainer;
    private Table source;
    private Table target;
    private Panel panel;
    boolean ignoreChange;
    
    public DragDropTwinCol(Class itemClass, String itemProperty, BeanItemContainer sourceContainer) {
        this.itemProperty = itemProperty;
        this.sourceContainer = sourceContainer;
        init(itemClass);
    }
    
    public DragDropTwinCol(Class itemClass, String itemProperty, Collection sourceItems) {
        this.itemProperty = itemProperty;
        this.sourceContainer = new BeanItemContainer<>(itemClass);
        sourceItems.stream().forEach((item) -> {
            sourceContainer.addBean(item);
        });
        init(itemClass);
    }
    
    private void init(Class itemClass) {
        source = new Table();
        target = new Table();
        panel = new Panel();
        ignoreChange = false;
        
        source.addContainerProperty(itemProperty, String.class, null, "Available", null, null);
        source.setContainerDataSource(sourceContainer);
        source.setVisibleColumns(itemProperty);
        source.setImmediate(true);
        source.setReadOnly(true);
        source.setPageLength(sourceContainer.getItemIds().size());
        source.setDragMode(Table.TableDragMode.ROW);
        source.setDropHandler(getDropHandler(source, new SourceIs(target)));

        target.addContainerProperty(itemProperty, String.class, null, "Assigned", null, null);
        BeanItemContainer targetContainer = new BeanItemContainer<>(itemClass);
        target.setContainerDataSource(targetContainer);
        target.setVisibleColumns(itemProperty);
        target.setImmediate(true);
        target.setReadOnly(true);
        target.setPageLength(sourceContainer.getItemIds().size());
        target.setDragMode(Table.TableDragMode.ROW);
        target.setDropHandler(getDropHandler(target, new SourceIs(source)));
        
    }

    @Override
    public void setReadOnly(boolean readOnly) {
        super.setReadOnly(readOnly);
        if (readOnly) {
            source.setDragMode(Table.TableDragMode.NONE);
            target.setDragMode(Table.TableDragMode.NONE);
        } else {
            source.setDragMode(Table.TableDragMode.ROW);
            target.setDragMode(Table.TableDragMode.ROW);
        }
    }
    
    @Override
    protected Component initContent() {
        GridLayout layout = new GridLayout(2, 1);
        layout.setMargin(true);
        layout.setSpacing(true);
        
        layout.addComponent(source);
        layout.addComponent(target);
        panel.setContent(layout);
        return panel;
    }

    @Override
    public Class<? extends Collection> getType() {
        return Collection.class;
    }
    
    public void addSourceListStyleName(String styleName) {
        source.addStyleName(styleName);
    }

    public void addTargetListStyleName(String styleName) {
        target.addStyleName(styleName);
    }
    
    public void addPanelStyleName(String styleName) {
        panel.addStyleName(styleName);
    }
    
    public void removeSourceListStyleName(String styleName) {
        source.removeStyleName(styleName);
    }

    public void removeTargetListStyleName(String styleName) {
        target.removeStyleName(styleName);
    }
    
    public void removePanelStyleName(String styleName) {
        panel.removeStyleName(styleName);
    }
    
    @Override
    public void addStyleName(String styleName) {
        panel.addStyleName(styleName);
        source.addStyleName(styleName);
        target.addStyleName(styleName);
    }
    
    @Override
    public void removeStyleName(String styleName) {
        panel.removeStyleName(styleName);
        source.removeStyleName(styleName);
        target.removeStyleName(styleName);
    }
    
    @Override
    public void setCaption(String caption) {
        panel.setCaption(caption);
    }

    public void setSourceListCaption(String caption) {
        source.setColumnHeader(itemProperty, caption);
    }
    
    public void setTargetListCaption(String caption) {
        target.setColumnHeader(itemProperty, caption);
    }
    
    public void setShowListCaptions(boolean show) {
        if (show) {
            source.setColumnHeaderMode(ColumnHeaderMode.EXPLICIT_DEFAULTS_ID);
            target.setColumnHeaderMode(ColumnHeaderMode.EXPLICIT_DEFAULTS_ID);
        } else {
            source.setColumnHeaderMode(ColumnHeaderMode.HIDDEN);
            target.setColumnHeaderMode(ColumnHeaderMode.HIDDEN);
        }
    }
    
    public void setListVisibleRows(int rows) {
        source.setPageLength(rows);
        target.setPageLength(rows);
    }
    
    public void setListColumnWidth(int width) {
        source.setColumnWidth(itemProperty, width);
        target.setColumnWidth(itemProperty, width);
    }
    
    private DropHandler getDropHandler(Table targetTable, ClientSideCriterion acceptCriterion) {
        return new DropHandler() {

            @Override
            public void drop(DragAndDropEvent dropEvent) {
                final DataBoundTransferable t = (DataBoundTransferable) dropEvent.getTransferable();
                if (t.getSourceContainer().equals(targetTable.getContainerDataSource())) {
                    return;
                }
                Container source = t.getSourceContainer();
                Object sourceItemId = t.getItemId();
                Container target = targetTable.getContainerDataSource();
                ((BeanItemContainer)target).addBean(((BeanItem)source.getItem(sourceItemId)).getBean());
                source.removeItem(sourceItemId);
                updateValues();
            }

            @Override
            public AcceptCriterion getAcceptCriterion() {
                return new And(acceptCriterion, AbstractSelect.AcceptItem.ALL);
            }
        };
    }

    @Override
    public void setPropertyDataSource(Property newDataSource) {
        super.setPropertyDataSource(newDataSource);
        resetValues();
        initValues((Collection) newDataSource.getValue());
        super.addValueChangeListener((Property.ValueChangeEvent event) -> {
            if (!ignoreChange) {
                resetValues();
                initValues((Collection) event.getProperty().getValue());
            }
        });
    }
    
    private void resetValues() {
        Collection sourceItems = new ArrayList();
        source.getContainerDataSource().getItemIds().stream().forEach((id) -> {
            sourceItems.add(((BeanItem)source.getContainerDataSource().getItem(id)).getBean());
        });
        target.getContainerDataSource().getItemIds().stream().forEach((id) -> {
            sourceItems.add(((BeanItem)target.getContainerDataSource().getItem(id)).getBean());
        });
        ((BeanItemContainer)source.getContainerDataSource()).removeAllItems();
        ((BeanItemContainer)target.getContainerDataSource()).removeAllItems();
        sourceItems.stream().forEach((item) -> {
            ((BeanItemContainer)source.getContainerDataSource()).addBean(item);
        });
    }
    
    private void initValues(Collection newValues) {
        if (newValues != null) {
            newValues.stream().forEach((item) -> {
                ((BeanItemContainer)target.getContainerDataSource()).addBean(item);
                ((BeanItemContainer)source.getContainerDataSource()).removeItem(item);
            });
        }
    }
    
    private void updateValues() {
        ignoreChange = true;
        setValue(getValue());
        ignoreChange = false;
    }

    @Override
    public List getValue() {
        List values = new ArrayList();
        ((BeanItemContainer)target.getContainerDataSource()).getItemIds().stream().forEach((id) -> {
            Object item = ((BeanItemContainer)target.getContainerDataSource()).getItem(id).getBean();
            values.add(item);
        });
        return values;
    }
    
    

}