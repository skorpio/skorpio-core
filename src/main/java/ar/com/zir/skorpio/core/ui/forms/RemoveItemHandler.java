/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.forms;

import ar.com.zir.skorpio.core.persistence.PersistenceException;

/**
 *
 * @author jmrunge
 */
public interface RemoveItemHandler {
    public void fireRemoveItem(Object item) throws PersistenceException;
}
