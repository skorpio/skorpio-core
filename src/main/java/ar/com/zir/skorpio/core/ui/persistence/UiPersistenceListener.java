/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.persistence;

/**
 *
 * @author jmrunge
 */
public interface UiPersistenceListener {
    public void fireUiUpdate(String action, Object item, boolean editing);
}
