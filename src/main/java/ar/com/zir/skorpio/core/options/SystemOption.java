/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.options;

import com.vaadin.server.FontAwesome;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 *
 * @author jmrunge
 */
public class SystemOption {
    private String id;
    private String name;
    private FontAwesome icon;
    private String resource;
    private String event;
    private SystemOption parent;
    private List<SystemOption> children;
    private Map<String, Boolean> actions;

    public SystemOption() {
    }
    
    private void _setChildren(List<SystemOption> children) {
        if (children != null) {
            children.stream().forEach((child) -> {
                addChild(child);
            });
        } else {
            this.children = null;
        }
    }

    private void _setActions(Map<String, Boolean> actions) {
        if (actions != null) {
            actions.keySet().stream().forEach((key) -> {
                addAction(key, actions.get(key));
            });
        } else {
            this.actions = null;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SystemOption getParent() {
        return parent;
    }

    public void setParent(SystemOption parent) {
        this.parent = parent;
    }

    public List<SystemOption> getChildren() {
        return children;
    }

    public void setChildren(List<SystemOption> children) {
        _setChildren(children);
    }

    public Map<String, Boolean> getActions() {
        return actions;
    }

    public void setActions(Map<String, Boolean> actions) {
        _setActions(actions);
    }

    public FontAwesome getIcon() {
        return icon;
    }

    public void setIcon(FontAwesome icon) {
        this.icon = icon;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SystemOption other = (SystemOption) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return name;
    }

    public void addChild(SystemOption child) {
        if (children == null)
            children = new ArrayList<>();
        child.setParent(this);
        children.add(child);
    }

    public void addAction(String action, boolean showInMenu) {
        if (actions == null)
            actions = new HashMap<>();
        actions.put(action, showInMenu);
    }
    
    public List<String> getMenuActions() {
        if (actions == null) return new ArrayList<>();
        return Arrays.asList(actions.entrySet().stream().filter(a -> a.getValue()).collect(Collectors.toMap(a -> a.getKey(), a -> a.getValue())).keySet().toArray(new String[0]));
    }
}
