/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui;

import ar.com.zir.skorpio.core.config.DbSystemConfig;
import ar.com.zir.skorpio.core.ui.main.SkorpioMainView;
import ar.com.zir.skorpio.core.ui.login.LoginView;
import ar.com.zir.skorpio.core.security.SystemUser;
import ar.com.zir.skorpio.core.ui.initialconfig.InitialConfigView;
import javax.enterprise.event.Observes;
import javax.inject.Inject;
import org.vaadin.addon.cdimvp.AbstractMVPPresenter;
import org.vaadin.addon.cdimvp.CDIEvent;
import org.vaadin.addon.cdimvp.ParameterDTO;

/**
 *
 * @author jmrunge
 */
@SuppressWarnings("serial")
public abstract class AbstractSkorpioPresenter extends AbstractMVPPresenter<SkorpioView> {
    public static final String SHOW_LOGIN = "show_login";
    public static final String SHOW_MAIN = "show_main";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";
    public static final String SHOW_USER_DATA = "show_user_data";
    public static final String SHOW_INITIAL_CONFIG = "show_initial_config";
    
    @Inject
    private DbSystemConfig config;

    protected void show_main(@Observes @CDIEvent(SHOW_MAIN) final ParameterDTO parameters) {        
        view.setMainView(SkorpioMainView.class);
    }

    protected void login(@Observes @CDIEvent(LOGIN) final ParameterDTO parameters) {
        view.login((SystemUser)parameters.getPrimaryParameter());
    }

    protected void show_login(@Observes @CDIEvent(SHOW_LOGIN) final ParameterDTO parameters) {
        if (config.initialConfigDone())
            view.setMainView(LoginView.class);
        else
            view.setMainView(InitialConfigView.class);
    }
    
    protected void logout(@Observes @CDIEvent(LOGOUT) final ParameterDTO parameters) {
        view.logout();
    }
    
    protected void show_user_data(@Observes @CDIEvent(SHOW_USER_DATA) final ParameterDTO parameters) {
        view.showUserData((Boolean)parameters.getPrimaryParameter());
    }

    @Override
    public void viewEntered() {
        // NOP
    }

}