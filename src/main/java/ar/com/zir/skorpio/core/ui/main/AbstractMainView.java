/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.main;

import ar.com.zir.skorpio.core.ui.SkorpioDockItem;
import ar.com.zir.skorpio.core.options.SystemOption;
import ar.com.zir.skorpio.core.ui.forms.FormFactory;
import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.ui.persistence.UIPersistenceService;
import ar.com.zir.skorpio.core.ui.SkorpioUI;
import ar.com.zir.skorpio.core.ui.forms.SkorpioListForm;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Inject;
import org.vaadin.addon.cdimvp.AbstractMVPView;

/**
 *
 * @author jmrunge
 */
@SuppressWarnings("serial")
public abstract class AbstractMainView extends AbstractMVPView implements SkorpioMainView, OpenWindowHandler {
    @Inject
    private SystemOptions options;
    @Inject
    private FormFactory formFactory;
    @Inject
    private UIPersistenceService uiPersistence;
    
    @PostConstruct
    protected void initView() {
        setSizeFull();
        
        SkorpioMainForm form = getMainForm();
        List<SystemOption> opts = options.getRootSystemOptions();
        form.init(opts);
        form.addDockListener((SkorpioDockItem item) -> {
            if (item.getSystemOption().getEvent() != null)
                fireViewEvent(item.getSystemOption().getEvent(), item.getSystemOption());
        });
        form.setOpenWindowHandler(this);
        setCompositionRoot(form);
        form.setSizeFull();
    }
    
    @Override
    public void showMenu(SystemOption option) {
        getMainForm().showMenu(option);
        uiPersistence.setSelectedOption(SkorpioUI.getCurrent().getUser(), option);
    }

    @Override
    public void enter() {
        showHome();
    }

    @Override
    public void showHome() {
        getMainForm().showHome();
    }

    @Override
    public void addWindow(SystemOption option, String action) {
        addWindow(option, action, null, false);
    }
    
    @Override
    public void addWindow(SystemOption option, String action, Object item, boolean edit) {
        SkorpioPersistedForm form = formFactory.getForm(option, action, item, edit);
        form.setOpenWindowHandler(this);
        getMainForm().addWindow(form);
        formFactory.handleFormSync(form, item, edit);
        formFactory.handleFormPersistence(form, option, action, item, edit);
        if (form instanceof SkorpioListForm) {
            ((SkorpioListForm)form).addAddItemListener(() -> {
                fireViewEvent(AbstractMainPresenter.ADD_WINDOW, option, SystemOptions.ACTION_ADD, null, true);
            });
            ((SkorpioListForm)form).addSelectItemListener((Object obj) -> {
                fireViewEvent(AbstractMainPresenter.ADD_WINDOW, option, SystemOptions.ACTION_EDIT, obj, false);
            });
        }
    }
    
    public abstract SkorpioMainForm getMainForm();

}