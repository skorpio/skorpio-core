/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.persistence;

import ar.com.zir.skorpio.core.options.SystemOption;
import ar.com.zir.skorpio.core.sync.SyncException;
import ar.com.zir.skorpio.core.ui.FormUpdateListener;
import ar.com.zir.skorpio.core.ui.forms.WindowClosedListener;
import ar.com.zir.skorpio.core.ui.main.OpenWindowHandler;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Notification;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vaadin.addon.cdimvp.ViewComponent;

/**
 *
 * @author jmrunge
 */
public abstract class SkorpioPersistedForm extends ViewComponent {
    private List<UiPersistenceListener> uiPersistenceListeners;
    private List<FormUpdateListener> formUpdateListeners;
    private List<WindowClosedListener> windowClosedListeners;
    protected WindowCloseHandler closeHandler;
    private String instanceId;
    private OpenWindowHandler openWindowHandler;
    
    public OpenWindowHandler getOpenWindowHandler() {
        return openWindowHandler;
    }

    public void setOpenWindowHandler(OpenWindowHandler openWindowHandler) {
        this.openWindowHandler = openWindowHandler;
    }
        
    public void openNewWindow(SystemOption option, String action, Object item, boolean editing) {
        getOpenWindowHandler().addWindow(option, action, item, editing);
    }
    
    public void addUiPersistenceListener(UiPersistenceListener listener) {
        if (uiPersistenceListeners == null)
            uiPersistenceListeners = new ArrayList<>();
        uiPersistenceListeners.add(listener);
    }

    public void removeUiPersistenceListener(UiPersistenceListener listener) {
        if (uiPersistenceListeners != null)
            uiPersistenceListeners.remove(listener);
    }

    protected void updateUiPersistence(String action, Object obj, boolean editing) {
        if (uiPersistenceListeners != null) {
            uiPersistenceListeners.stream().forEach((listener) -> {
                listener.fireUiUpdate(action, obj, editing);
            });
        }
    }
        
    public void addFormUpdateListener(FormUpdateListener listener) {
        if (formUpdateListeners == null)
            formUpdateListeners = new ArrayList<>();
        formUpdateListeners.add(listener);
    }

    public void removeFormUpdateListener(FormUpdateListener listener) {
        if (formUpdateListeners != null)
            formUpdateListeners.remove(listener);
    }

    protected void fireTitleChanged(String newTitle) {
        if (formUpdateListeners != null) {
            formUpdateListeners.stream().forEach((listener) -> {
                listener.titleChanged(newTitle);
            });
        }
    }

    public void addWindowClosedListener(WindowClosedListener listener) {
        if (windowClosedListeners == null)
            windowClosedListeners = new ArrayList<>();
        windowClosedListeners.add(listener);
    }

    public void removeWindowClosedListener(WindowClosedListener listener) {
        if (windowClosedListeners != null)
            windowClosedListeners.remove(listener);
    }

    protected void fireWindowClosed(Object item) {
        if (windowClosedListeners != null) {
            windowClosedListeners.stream().forEach((listener) -> {
                try {
                    listener.fireWindowClosed(item);
                } catch (SyncException ex) {
                    Logger.getLogger(SkorpioPersistedForm.class.getName()).log(Level.SEVERE, null, ex);
                    Notification.show("Error", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
                }
            });
        }
    }
        
    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }
    
    public abstract FontAwesome getFormIcon();
    public abstract String getFormTitle();
 
    public void setCloseHandler(WindowCloseHandler handler) {
        closeHandler = handler;
    }
    
    public void requestClose() {
        closeHandler.closeWindow();
    }
        
    public void close() {
        fireWindowClosed(null);
    };
    
    public interface WindowCloseHandler {
        public void closeWindow();
    }
}
