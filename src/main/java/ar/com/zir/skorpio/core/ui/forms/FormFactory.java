/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ar.com.zir.skorpio.core.ui.forms;

import ar.com.zir.skorpio.core.options.SystemOption;
import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.persistence.InternalEntity;
import ar.com.zir.skorpio.core.sync.ObjectSyncListener;
import ar.com.zir.skorpio.core.sync.SkorpioSyncService;
import ar.com.zir.skorpio.core.sync.SyncException;
import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import ar.com.zir.skorpio.core.sync.SynchronizedObject;
import ar.com.zir.skorpio.core.ui.SkorpioUI;
import ar.com.zir.skorpio.core.ui.main.AbstractMainView;
import ar.com.zir.skorpio.core.ui.persistence.SkorpioPersistedForm;
import ar.com.zir.skorpio.core.ui.persistence.UIPersistenceService;
import com.vaadin.data.util.BeanItem;
import com.vaadin.ui.Notification;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.inject.Inject;

/**
 *
 * @author jmrunge
 */
public abstract class FormFactory {
    @Inject
    private SkorpioSyncService syncService;
    @Inject
    private UIPersistenceService uiPersistence;

    public SkorpioPersistedForm getForm(SystemOption option, String action, Object obj, boolean editing) {
        FormProvider provider = getFormProvider(option, action, obj);
        SkorpioPersistedForm form = provider.getFormInstance(action);
        boolean canDelete = option.getActions().containsKey(SystemOptions.ACTION_REMOVE);
        boolean canEdit = option.getActions().containsKey(SystemOptions.ACTION_EDIT);
        boolean canAdd = option.getActions().containsKey(SystemOptions.ACTION_ADD);
        boolean canView = option.getActions().containsKey(SystemOptions.ACTION_VIEW);
        if (obj != null && obj instanceof InternalEntity) {
            if (canDelete)
                canDelete = !((InternalEntity)obj).getInternal();
            if (canEdit)
                canEdit = !((InternalEntity)obj).getInternal();
        }
        if (form instanceof SkorpioListForm) {
            List<SkorpioListColumn> columns = provider.getFormListColumns();
            SkorpioBeanItemContainer container = provider.getFormContainer();
            ((SkorpioListForm)form).setRemoveItemHandler(getFormRemoveHandlerForOption(provider));
            ((SkorpioListForm)form).initForm(option.getName(), option.getIcon(), columns, container, (Map<String, Object>) obj, provider.getObjectClass(), action, canAdd, canView, canDelete);
            ((SkorpioListForm)form).setObjects(provider.getTableObjects());
            ((SkorpioListForm)form).setUiUpdateHandler((Object item) -> {
                try {
                    item = provider.getFreshObject(item);
                } catch (PersistenceException ex) {
                    Notification.show("Error refreshing object", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
                    Logger.getLogger(FormFactory.class.getName()).log(Level.SEVERE, null, ex);
                }
                ((SkorpioListForm)form).updateItem((SynchronizableObject) item);
            });            
        } else if (form instanceof SkorpioAbmForm) {
            List<SkorpioAbmField> fields = provider.getFormFields();
            String title;
            if (obj == null) {
                title = "Nuevo";
                obj = provider.getNewObject();
            } else {
                title = obj.toString();
                if (!editing) {
                    try {
                        obj = provider.getFreshObject(obj);
                    } catch (PersistenceException ex) {
                        Notification.show("Error refreshing object", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
                        Logger.getLogger(FormFactory.class.getName()).log(Level.SEVERE, "Error refreshing object", ex);
                    }
                }
            }
            BeanItem beanItem = new BeanItem<>(obj);
            ((SkorpioAbmForm)form).initForm(option.getName() + " [" + title + "]", option.getIcon(), fields, action, canEdit, canDelete, editing);
            ((SkorpioAbmForm)form).setBean(beanItem);
            ((SkorpioAbmForm)form).setRemoveItemHandler(getFormRemoveHandlerForOption(provider));
            ((SkorpioAbmForm)form).setSaveItemHandler(getFormSaveItemHandlerForOption(provider, option.getName(), (SkorpioAbmForm)form));
            ((SkorpioAbmForm)form).setUiUpdateHandler((Object item) -> {
                try {
                    item = provider.getFreshObject(item);
                } catch (PersistenceException ex) {
                    Notification.show("Error refreshing object", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
                    Logger.getLogger(FormFactory.class.getName()).log(Level.SEVERE, null, ex);
                }
                ((SkorpioAbmForm)form).updateForm(option.getName() + " [" + item.toString() + "]", SystemOptions.ACTION_EDIT, new BeanItem<>(item));
            });            
        }
        return form;
    }
    
    public void handleFormSync(SkorpioPersistedForm form, Object obj, boolean editing) {
        if (editing && (obj != null) && (obj instanceof SynchronizableObject) && (((SynchronizableObject)obj).getId() != null)) {
            try {
                syncService.updateLockedObjectOwner((SynchronizableObject)obj, SkorpioUI.getCurrent().getUser(), form.getInstanceId());
            } catch (SyncException ex) {
                Logger.getLogger(AbstractMainView.class.getName()).log(Level.SEVERE, null, ex);
                Notification.show("Error updating object owner", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
        }
        if (form instanceof ObjectSyncListener) {
            SkorpioUI.getCurrent().addListener((ObjectSyncListener)form);
        }
        if (form instanceof SkorpioListForm) {
            ((SkorpioListForm)form).addRemovedItemListener(getRemovedListener(form));
            List<SynchronizedObject> lockedObjects = syncService.getLockedObjects(((ObjectSyncListener)form).getObjectClass());
            if (lockedObjects != null) {
                lockedObjects.stream().forEach((o) -> {
                    ((SkorpioListForm)form).objectLocked(o);
                });
            }
        } else if (form instanceof SkorpioAbmForm) {
            ((SkorpioAbmForm)form).addEditItemListener((Object item1) -> {
                syncService.lockObject((SynchronizableObject)item1, SkorpioUI.getCurrent().getUser(), form.getInstanceId());
            });
            ((SkorpioAbmForm)form).addSavedItemListener((String currentAction, Object item1) -> {
                switch (currentAction) {
                    case SystemOptions.ACTION_ADD:
                        syncService.addObject((SynchronizableObject) item1, SkorpioUI.getCurrent().getUser(), form.getInstanceId());
                        break;
                    case SystemOptions.ACTION_EDIT:
                        syncService.releaseObject((SynchronizableObject) item1, SkorpioUI.getCurrent().getUser());
                        break;
                }
            });
            ((SkorpioAbmForm)form).addRemovedItemListener(getRemovedListener(form));
            if (obj != null) {
                SynchronizedObject locked = syncService.getLockedObject((SynchronizableObject) obj);
                if (locked != null)
                    ((SkorpioAbmForm)form).objectLocked(locked);
            }
        }
        form.addWindowClosedListener((Object item2) -> {
            if (form instanceof ObjectSyncListener)
                SkorpioUI.getCurrent().removeListener((ObjectSyncListener) form);
            if (item2 != null && ((SynchronizableObject)item2).getId() != null)
                syncService.releaseObject((SynchronizableObject)item2, SkorpioUI.getCurrent().getUser());
        });
    }
    
    public void handleFormPersistence(SkorpioPersistedForm form, SystemOption option, String action, Object obj, boolean editing) {
        uiPersistence.addWindow(SkorpioUI.getCurrent().getUser(), option, form.getInstanceId(), action, obj, editing);
        form.addUiPersistenceListener((String newAction, Object value, boolean edit) -> {
            uiPersistence.updateWindow(SkorpioUI.getCurrent().getUser(), option, form.getInstanceId(), newAction, value, edit);
        });
        form.addWindowClosedListener((Object item2) -> {
            uiPersistence.removeWindow(SkorpioUI.getCurrent().getUser(), form.getInstanceId());
        });
    }
    
    private RemovedItemListener getRemovedListener(SkorpioPersistedForm form) { 
        return ((Object obj) -> {
            syncService.removeObject((SynchronizableObject)obj, SkorpioUI.getCurrent().getUser(), form.getInstanceId());
        });
    }
    
    private RemoveItemHandler getFormRemoveHandlerForOption(FormProvider provider) {
        return (Object item) -> {
            provider.doRemoveItem(item, SkorpioUI.getCurrent().getUser());
        };
    };
    
    private SaveItemHandler getFormSaveItemHandlerForOption(FormProvider provider, String optionName, SkorpioAbmForm form) {
        return (String currentAction, Object item) -> {
            Object updated;
            switch (currentAction) {
                case SystemOptions.ACTION_ADD:
                    updated = provider.doCreateObject(item, SkorpioUI.getCurrent().getUser());
                    break;
                case SystemOptions.ACTION_EDIT:
                    updated = provider.doUpdateObject(item, SkorpioUI.getCurrent().getUser());
                    break;
                default:
                    updated = null;
            }
            form.updateForm(optionName + " [" + updated.toString() + "]", SystemOptions.ACTION_EDIT, new BeanItem<>(updated));
        };
    }

    protected abstract FormProvider getFormProvider(SystemOption option, String action, Object item);

}
