/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

/**
 *
 * @author jmrunge
 */
public class SkorpioAbmComboField extends SkorpioAbmField {
    private String inputPrompt;
    private String itemCaptionProp;
    private String dependantProp;
    private String dependantValuesProp;
    private NewItemHandler newItemHandler;

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, true, new String[0]);
    }
    
    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, true, styles);
    }
    
    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, NewItemHandler newItemHandler) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, true, newItemHandler, null);
    }
    
    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, NewItemHandler newItemHandler, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, true, newItemHandler, styles);
    }
    
    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, boolean bind) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, bind, new String[0]);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, boolean bind, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, bind, styles);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, boolean bind, NewItemHandler newItemHandler) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, bind, newItemHandler, null);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, boolean bind, NewItemHandler newItemHandler, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, null, null, bind, newItemHandler, styles);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, true, new String[0]);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, true, styles);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, NewItemHandler newItemHandler) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, true, newItemHandler, null);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, NewItemHandler newItemHandler, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, true, newItemHandler, styles);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, boolean bind) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, bind, null, null);
    }
    
    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, boolean bind, String[] styles) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, bind, null, styles);
    }
    
    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, boolean bind, NewItemHandler newItemHandler) {
        this(caption, field, required, inputPrompt, itemCaptionProp, dependantProp, dependantValuesProp, bind, newItemHandler, null);
    }

    public SkorpioAbmComboField(String caption, String field, boolean required, String inputPrompt, String itemCaptionProp, String dependantProp, String dependantValuesProp, boolean bind, NewItemHandler newItemHandler, String[] styles) {
        super(caption, field, required, bind, styles);
        this.inputPrompt = inputPrompt;
        this.itemCaptionProp = itemCaptionProp;
        this.dependantProp = dependantProp;
        this.dependantValuesProp = dependantValuesProp;
        this.newItemHandler = newItemHandler;
    }

    public String getInputPrompt() {
        return inputPrompt;
    }

    public void setInputPrompt(String inputPrompt) {
        this.inputPrompt = inputPrompt;
    }

    public String getItemCaptionProp() {
        return itemCaptionProp;
    }

    public void setItemCaptionProp(String itemCaptionProp) {
        this.itemCaptionProp = itemCaptionProp;
    }

    public String getDependantProp() {
        return dependantProp;
    }

    public void setDependantProp(String dependantProp) {
        this.dependantProp = dependantProp;
    }

    public String getDependantValuesProp() {
        return dependantValuesProp;
    }

    public void setDependantValuesProp(String dependantValuesProp) {
        this.dependantValuesProp = dependantValuesProp;
    }

    public NewItemHandler getNewItemHandler() {
        return newItemHandler;
    }

    public void setNewItemHandler(NewItemHandler newItemHandler) {
        this.newItemHandler = newItemHandler;
    }
    
}
