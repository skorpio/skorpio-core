/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.sync;

/**
 *
 * @author jmrunge
 */
public interface ObjectSyncListener extends SyncListener {
    public Class getObjectClass();
}
