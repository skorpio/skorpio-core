/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ar.com.zir.skorpio.core.ui.forms;

import ar.com.zir.skorpio.core.persistence.PersistenceException;
import ar.com.zir.skorpio.core.options.SystemOptions;
import ar.com.zir.skorpio.core.sync.ObjectSyncListener;
import ar.com.zir.skorpio.core.sync.SyncException;
import ar.com.zir.skorpio.core.sync.SynchronizableObject;
import ar.com.zir.skorpio.core.sync.SynchronizedObject;
import ar.com.zir.skorpio.core.ui.AbstractSkorpioForm;
import ar.com.zir.skorpio.core.ui.SkorpioUI;
import com.vaadin.data.Property;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.MouseEvents;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.ThemeResource;
import com.vaadin.shared.ui.combobox.FilteringMode;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbsoluteLayout;
import com.vaadin.ui.AbstractField;
import com.vaadin.ui.AbstractSelect;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.vaadin.dialogs.ConfirmDialog;
import tm.kod.widgets.numberfield.NumberField;

/**
 *
 * @author jmrunge
 */
public class SkorpioAbmForm extends AbstractSkorpioForm implements ObjectSyncListener {
    private RemoveItemHandler removeHandler;
    private SaveItemHandler saveHandler;
    private List<SavedItemListener> savedListeners;
    private List<EditItemListener> editListeners;
    private UiUpdateHandler uiUpdateHandler;
    private List<RemovedItemListener> removedListeners;
    protected GridLayout grid;
    private List<SkorpioAbmField> fields;
//    private Image embImage;
    private Label lblTitle;
    private Label lblInfo;
    private Embedded embDelete;
    private String title;
    private FontAwesome icon;
    private Embedded embEdit;
    private Embedded embSave;
    private Embedded embLock;
    private MouseEvents.ClickListener saveClickListener;
    private MouseEvents.ClickListener deleteClickListener;
    private MouseEvents.ClickListener editClickListener;
    protected String action;
    private boolean canEdit;
    private boolean canDelete;
    private boolean editing;
    private Map<String, BeanItemContainer> containers = new HashMap<>();
    private Map<String, AbstractField> formFields = new HashMap<>();

    public SkorpioAbmForm() {
        this(4,1);
    }
    
    public SkorpioAbmForm(int width, int height) {
        VerticalLayout mainLayout = buildMainLayout(width, height);
        setCompositionRoot(mainLayout);
    }

    public void initForm(String title, FontAwesome icon, List<SkorpioAbmField> fields, String action, boolean canEdit, boolean canDelete, boolean editing) {
        this.action = action;
        this.canEdit = canEdit;
        this.canDelete = canDelete;
        this.editing = editing;
        this.title = title;
        this.icon = icon;
//        embImage.setSource(icon);
        lblTitle.setValue("<h2>" + icon.getHtml() + " " + title + "</h2>");
        this.fields = fields;
        deleteClickListener = (MouseEvents.ClickEvent event) -> {
            ConfirmDialog d = ConfirmDialog.show(SkorpioUI.getCurrent(), "Eliminar item", "¿Desea eliminar el item seleccionado?", "Sí", "No", (ConfirmDialog cd) -> {
                if (cd.isConfirmed()) {
                    try {
                        removeItem((BeanItem)getBeanItem());
                        Notification.show("Éxito", "El item se eliminó exitosamente", Notification.Type.HUMANIZED_MESSAGE);
                        requestClose();
                    } catch(PersistenceException | SyncException ex) {
                        Notification.show("Error", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
                    }
                }
            });
        };
        saveClickListener = (MouseEvents.ClickEvent event) -> {
            SkorpioAbmForm.this.commit();
        };
        editClickListener = (MouseEvents.ClickEvent event) -> {
            try {
                editItem();
                enableEditing();
            } catch (SyncException ex) {
                Notification.show("Error obteniendo lock", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
            }
        };
        if (!canDelete || action.equals(SystemOptions.ACTION_ADD)) {
            embDelete.setSource(new ThemeResource("remove_disabled.png"));
        } else {
            embDelete.addClickListener(deleteClickListener);
        }
        embSave.setSource(new ThemeResource("save_disabled.png"));
        if (!canEdit) {
            embEdit.setSource(new ThemeResource("edit_disabled.png"));
        } else {
            embEdit.addClickListener(editClickListener);
        }
        addCommitListener((BeanItem bean) -> {
            ConfirmDialog d = ConfirmDialog.show(SkorpioUI.getCurrent(), "Grabar item", "¿Desea grabar el item?", "Sí", "No", (ConfirmDialog cd) -> {
                if (cd.isConfirmed()) {
                    try {
                        saveItem(bean);
                        Notification.show("Éxito", "El item se grabó exitosamente", Notification.Type.HUMANIZED_MESSAGE);
                        disableEditing();
                    } catch(PersistenceException | SyncException ex) {
                        Notification.show("Error", ex.getMessage(), Notification.Type.ERROR_MESSAGE);
                    }
                }
            });
        });
    }
    
    protected boolean isEditing() {
        return editing;
    }
    
    protected void registerContainer(String property, BeanItemContainer container) {
        containers.put(property, container);
    }
    
    protected BeanItemContainer getContainer(String property) {
        return containers.get(property);
    }
    
    protected void registerField(String property, AbstractField field) {
        formFields.put(property, field);
    }
    
    protected AbstractField getField(String property) {
        return formFields.get(property);
    }
    
    public Object getFieldValue(String property) {
        return getField(property).getValue();
    }
    
    public void setFieldValue(String property, Object newValue) {
        boolean readOnly = formFields.get(property).isReadOnly();
        if (readOnly)
            getField(property).setReadOnly(false);
        getField(property).setValue(newValue);
        if (readOnly)
            getField(property).setReadOnly(true);
    }
    
    public Collection getContainerValues(String property) {
        Collection values = new ArrayList();
        BeanItemContainer container = getContainer(property);
        for (Object id : container.getItemIds()) {
            values.add(container.getItem(id).getBean());
        }
        return values;
    }
    
    protected Object getContainerBeanItemId(SynchronizableObject obj, String propName) {
        BeanItemContainer container = getContainer(propName);
        for (Object id : container.getItemIds()) {
            SynchronizableObject old = (SynchronizableObject)container.getItem(id).getBean();
            if (old.getId().equals(obj.getId())) {
                return id;
            }
        }
        System.out.println("Object " + obj + " not found");
        return null;
    }
    
    public boolean updateContainerItem(String property, SynchronizableObject obj) {
        Object id = getContainerBeanItemId(obj, property);
        if (id != null) {
            BeanItem item = getContainer(property).getItem(id);
            BeanItem newItem = new BeanItem(obj);
            for (Object key : item.getItemPropertyIds()) {
                Property prop = item.getItemProperty(key);
                if (!prop.isReadOnly()) {
                    System.out.println("Setting value for property " + key + " - " + prop.getValue());
                    if (newItem.getItemProperty(key) != null) {
                        System.out.println("New value: " + newItem.getItemProperty(key).getValue());
                        prop.setValue(newItem.getItemProperty(key).getValue());
                    } else {
                        System.out.println("Property " + key + " not found");
                    }
                } else {
                    System.out.println("Property " + key + " is readonly");
                }
            }
            if (getField(property) != null && getField(property).getValue() != null && getField(property).getValue().equals(obj))
                return true;
            if (!editing)
                getField(property).setReadOnly(true);
        }
        return false;
    }
    
    public Object resetComboItems(String property, Collection newItems) {
        Object ret = null;
        if (!property.contains(".")) {
            Property prop = getBeanItem().getItemProperty(property);
            SynchronizableObject obj = (SynchronizableObject) prop.getValue();
            getContainer(property).removeAllItems();
            getContainer(property).addAll(newItems);
            if (newItems.contains(obj)) {
                prop.setValue(obj);
                ret = obj;
            } else {
                prop.setValue(null);
            }
        } else {
            AbstractField field = getField(property);
            SynchronizableObject obj = (SynchronizableObject) field.getValue();
            getContainer(property).removeAllItems();
            getContainer(property).addAll(newItems);
            if (newItems.contains(obj)) 
                ret = obj;
            setFieldValue(property, ret);
        }
        if (!editing)
            getField(property).setReadOnly(true);
        return ret;
    }
    
    public void removeContainerItem(String property, SynchronizableObject obj) {
        Object id = getContainerBeanItemId(obj, property);
        if (id != null) {
            getContainer(property).removeItem(id);
            if (getField(property) instanceof Table)
                ((Table)getField(property)).setPageLength(getContainer(property).getItemIds().size());
        }
        if (!editing)
            getField(property).setReadOnly(true);
    }
    
    public void addContainerItem(String property, SynchronizableObject obj) {
        getContainer(property).addBean(obj);
        if (getField(property) instanceof Table)
            ((Table)getField(property)).setPageLength(getContainer(property).getItemIds().size());
        if (!editing)
            getField(property).setReadOnly(true);
    }
    
    public Object getFormObject() {
        return getBeanItem().getBean();
    }
        
    protected ComboBox getComboBox(SkorpioAbmComboField field, FieldGroup binder) {
        ComboBox combo = new ComboBox(field.getCaption(), getContainer(field.getField()));
        binder.bind(combo, field.getField());
        combo.setRequired(field.isRequired());
        combo.setImmediate(true);
        combo.setReadOnly(true);
        combo.addStyleName("visible");
        combo.setInputPrompt(field.getInputPrompt());
        if (field.getItemCaptionProp() != null) {
            combo.setItemCaptionPropertyId(field.getItemCaptionProp());
            combo.setItemCaptionMode(AbstractSelect.ItemCaptionMode.PROPERTY);
        }
        combo.setFilteringMode(FilteringMode.CONTAINS);
        combo.setNullSelectionAllowed(!field.isRequired());
        combo.setNewItemsAllowed(field.getNewItemHandler() != null);
        combo.setNewItemHandler((String newItemCaption) -> {
            field.getNewItemHandler().addNewItem(this, newItemCaption);
        });
        combo.addValueChangeListener((Property.ValueChangeEvent event) -> {
            if (field.getDependantProp() != null) {
                if (combo.getValue() != null)
                    resetComboItems(field.getDependantProp(), getBeanItemContainerItems(field.getDependantValuesProp(), combo.getValue()));
                else
                    resetComboItems(field.getDependantProp(), new ArrayList());
            }
            
            updateUiPersistence(action, getBeanItem().getBean(), true);
        }); 
        if (field.getDependantProp() != null)
            binder.unbind(combo);
        registerField(field.getField(), combo);
        return combo;
    }
    
    private SkorpioAbmField getSkorpioAbmField(String field) {
        for (SkorpioAbmField fld : fields) {
            if (fld.getField().equals(field))
                return fld;
        }
        return null;
    }
    
    protected void enableEditing() {
        embSave.setSource(new ThemeResource("save.png"));
        embSave.addClickListener(saveClickListener);
        embEdit.setSource(new ThemeResource("edit_disabled.png"));
        embEdit.removeClickListener(editClickListener);
        embDelete.setSource(new ThemeResource("remove_disabled.png"));
        embDelete.removeClickListener(deleteClickListener);
        fields.forEach((SkorpioAbmField field) -> {
            if (field.isEditable()) {
                Component c = getField(field.getField());
                if (c instanceof AbstractField) {
                    ((AbstractField)c).setReadOnly(false);
                    c.removeStyleName("visible");
                }
            }
        });
        editing = true;
    }
    
    protected void disableEditing() {
        embSave.setSource(new ThemeResource("save_disabled.png"));
        embSave.removeClickListener(saveClickListener);
        if (canEdit) {
            embEdit.setSource(new ThemeResource("edit.png"));
            embEdit.addClickListener(editClickListener);
        }
        if (canDelete) {
            embDelete.setSource(new ThemeResource("remove.png"));
            embDelete.addClickListener(deleteClickListener);
        }
        fields.forEach((SkorpioAbmField field) -> {
            Component c = getField(field.getField());
            if (c instanceof AbstractField) {
                ((AbstractField)c).setReadOnly(true);
                c.addStyleName("visible");
            }
        });
        editing = false;
    }
    
    @Override
    public String getFormTitle() {
        return title;
    }
    
    @Override
    public FontAwesome getFormIcon() {
        return icon;
    }
    
    @Override
    public void requestClose() {
        if (editing) {
            ConfirmDialog d = ConfirmDialog.show(SkorpioUI.getCurrent(), "Cerrar", "Si cierra la ventana perderá todos los cambios. ¿Desea cerrar la ventana igualmente?", "Sí", "No", (ConfirmDialog cd) -> {
                if (cd.isConfirmed())
                    closeHandler.closeWindow();
            });
            d.setWidth("25em");
        } else {
            closeHandler.closeWindow();
        }
    }
    
    @Override
    public void close() {
        if (editing)
            fireWindowClosed(getBeanItem().getBean());
        else
            fireWindowClosed(null);
    }
    
    public void setUiUpdateHandler(UiUpdateHandler handler) {
        uiUpdateHandler = handler;
    }
    
    public void updateForm(String title, String action, BeanItem bean) {
        lblTitle.setValue("<h2>" + icon.getHtml() + " " + title + "</h2>");
        this.title = title;
        fireTitleChanged(title);
        this.action = action;
        updateFormBeanItem(bean);
    }
    
    public void updateFormBeanItem(BeanItem bean) {
        getBinder().setItemDataSource(bean);
        updateAdditionalFields(bean);
        updateUiPersistence(action, bean.getBean(), false);
        if (!editing)
            disableEditing();
    }
    
    protected void updateAdditionalFields(BeanItem bean) {}
    
    protected Class getBeanItemContainerClass(String property) {
        try {
//            Field beanField = getBeanItem().getBean().getClass().getDeclaredField(property);
            Field beanField = getBeanField(property);
            ParameterizedType listType = (ParameterizedType) beanField.getGenericType();
            Class<?> listClass = (Class<?>) listType.getActualTypeArguments()[0];
            return listClass;
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(SkorpioAbmForm.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    private Field getBeanField(String property) throws NoSuchFieldException {
        for (Field f : getAllClassFields(null, getBeanItem().getBean().getClass())) {
            if (f.getName().equals(property))
                return f;
        }
        throw new NoSuchFieldException("No field with name " + property + " found");
    }
    
    private List<Field> getAllClassFields(List<Field> _fields, Class<?> type) {
        if (_fields == null)
            _fields = new ArrayList<>();
        
        _fields.addAll(Arrays.asList(type.getDeclaredFields()));

        if (type.getSuperclass() != null) {
            getAllClassFields(_fields, type.getSuperclass());
        }

        return _fields;
    }
    
    protected List getBeanItemContainerItems(String property) {
        return getBeanItemContainerItems(property, getBeanItem().getBean());
    }
    
    protected List getBeanItemContainerItems(String property, Object obj) {
        try {
            Method m = obj.getClass().getMethod("get" + property.substring(0, 1).toUpperCase() + property.substring(1), (Class<?>[]) null);
            return (List) m.invoke(obj, (Object[]) null);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException | NoSuchMethodException | InvocationTargetException ex) {
            Logger.getLogger(SkorpioAbmForm.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    protected Panel getTable(SkorpioAbmTableField field) {
        Table table = new Table();
        BeanItemContainer container = new BeanItemContainer(getBeanItemContainerClass(field.getField()));
        List items = getBeanItemContainerItems(field.getField());
        if (items != null) {
            items.stream().forEach((item) -> {
                container.addBean(item);
            });
        }
        registerContainer(field.getField(), container);
        List visibleCols = new ArrayList();
        for (SkorpioListColumn col : field.getColumns()) {
            if (col.getId().contains("."))
                container.addNestedContainerProperty(col.getId());
            table.addContainerProperty(col.getId(), col.getClazz(), null, col.getTitle(), null, null);
            visibleCols.add(col.getId());
            if (col.getClazz().equals(Boolean.class)) {
                table.addGeneratedColumn(col.getId(), (Table source, Object itemId, Object columnId) -> {
                    Property prop = table.getItem(itemId).getItemProperty(columnId);
                    if (table.isEditable()) {
                        return new CheckBox(null, prop);
                    } else {
                        if (prop.getValue() != null) 
                            return ((Boolean)prop.getValue()) ? new Label("<center>" + FontAwesome.CHECK_SQUARE_O.getHtml() + "</center>", ContentMode.HTML) : new Label("<center>" + FontAwesome.SQUARE_O.getHtml() + "</center>", ContentMode.HTML);
                        else
                            return null;
                    }
                });
                table.setColumnAlignment(col.getId(), Table.Align.CENTER);
            }
        }
        table.setContainerDataSource(container);
        table.setVisibleColumns(visibleCols.toArray());
        table.setImmediate(true);
        table.setReadOnly(true);
        table.setPageLength(items != null ? items.size() : 0);
        table.addValueChangeListener((Property.ValueChangeEvent event) -> {
            updateUiPersistence(action, getBeanItem().getBean(), true);
        });
        table.addStyleName("visible");
        Panel wrapper = new Panel(field.getCaption(), table);
        wrapper.addStyleName("visible");
        registerField(field.getField(), table);
        return wrapper;
    }
    
    private Field getDeclaredField(Class clazz, String field) {
        try {
            return clazz.getDeclaredField(field);
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(SkorpioAbmForm.class.getName()).log(Level.SEVERE, "Error looking for field " + field, ex);
            return null;
        }
    }
    
    protected AbstractField getAbstractField(SkorpioAbmField field, FieldGroup binder) {
        AbstractField f = null;
        boolean binded = false;
        //TODO: Arreglar esta mierda para que se banque nested propertys (y el else if tambien)
        if (!field.getField().contains(".") && binder.getItemDataSource().getItemProperty(field.getField()).getType().equals(BigDecimal.class)) {
            //if BigDecimal, attemp to create a DecimalTextField
            try {
                //Get item from binder
                Object item = ((BeanFieldGroup)binder).getItemDataSource().getBean();
                //Attemp to get field representing prop from item's class
                Field fld = getDeclaredField(item.getClass(), field.getField());
                //If not found, keep searching superclasses until is found or no superclass else to search
                while (fld == null) {
                    Class clazz = item.getClass().getSuperclass();
                    if (clazz == null)
                        break;
                    fld = getDeclaredField(clazz, field.getField());
                }
                if (fld != null) {
                    f = new NumberField(field.getCaption());
                    ((NumberField)f).setSigned(true);
                    ((NumberField)f).setUseGrouping(false);
//                    f.setGroupingSeparator(' ');
                    ((NumberField)f).setDecimal(true);
                    ((NumberField)f).setDecimalSeparator('.');
                    ((NumberField)f).addStyleName("numerical");
                    f.setLocale(Locale.ENGLISH);
                    binder.bind(f, field.getField());
                    binded = true;
                }
            } catch (SecurityException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error setting DecimalFormat to " + field.getField(), ex);
            }
        } else if (!field.getField().contains(".") && binder.getItemDataSource().getItemProperty(field.getField()).getType().equals(Integer.class)) {
            //if Integer, attemp to create a IntegerTextField
            try {
                //Get item from binder
                Object item = ((BeanFieldGroup)binder).getItemDataSource().getBean();
                //Attemp to get field representing prop from item's class
                Field fld = getDeclaredField(item.getClass(), field.getField());
                //If not found, keep searching superclasses until is found or no superclass else to search
                while (fld == null) {
                    Class clazz = item.getClass().getSuperclass();
                    if (clazz == null)
                        break;
                    fld = getDeclaredField(clazz, field.getField());
                }
                if (fld != null) {
                    f = new NumberField(field.getCaption());
                    ((NumberField)f).setSigned(true);
                    ((NumberField)f).setUseGrouping(false);
//                    f.setGroupingSeparator(' ');
                    ((NumberField)f).setDecimal(false);
//                    f.setDecimalSeparator('.');
                    ((NumberField)f).addStyleName("numerical");
                    f.setLocale(Locale.ENGLISH);
                    binder.bind(f, field.getField());
                    binded = true;
                }
            } catch (SecurityException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Error setting MaxIntegers to " + field.getField(), ex);
            }
        } 
        if (!binded) {
            //If not BigDecimal or decimal format config failed, delegate to binder
            if (field.getFieldClass() == null)
                f = (AbstractField) binder.buildAndBind(field.getCaption(), field.getField());
            else
                f = (AbstractField) binder.buildAndBind(field.getCaption(), field.getField(), field.getFieldClass());
        }
        f.setRequired(field.isRequired());
        f.setImmediate(true);
        f.setReadOnly(true);
        f.addValueChangeListener((Property.ValueChangeEvent event) -> {
            updateUiPersistence(action, getBeanItem().getBean(), true);
        });
        f.addStyleName("visible");
        registerField(field.getField(), f);
        return f;
    }
    
    @Override
    protected void bindBean(FieldGroup binder) {
        
        fields.stream().forEach((field) -> {
            if (field.isBind()) {
                Component c;
                if (field instanceof SkorpioAbmTableField) {
                    c = getTable((SkorpioAbmTableField) field);
                    attachComponent(field, c);
                } else if (field instanceof SkorpioAbmComboField) {
                    ComboBox combo = getComboBox((SkorpioAbmComboField) field, binder);
                    c = combo;
                    attachComponent(field, combo);
                    if (((SkorpioAbmComboField)field).getDependantProp() != null)
                        getContainer(((SkorpioAbmComboField)field).getDependantProp()).addAll(getBeanItemContainerItems(((SkorpioAbmComboField)field).getDependantValuesProp(), combo.getValue()));
                } else {
                    c = getAbstractField(field, binder);
                    attachComponent(field, c);
                } 
                if (field.getStyles() != null && field.getStyles().length != 0) {
                    for (String style : field.getStyles()) {
                        c.addStyleName(style);
                    }
                }
            } else {
                processField(field, binder);
            }
        });
        bindAdditionalFields(binder);
        updateUiPersistence(action, getBeanItem().getBean(), action.equals(SystemOptions.ACTION_ADD));
        
        if (editing)
            enableEditing();
    }
    
    protected void attachComponent(SkorpioAbmField field, Component comp) {
        if (field.getLocation() == null)
            grid.addComponent(comp);
        else
            grid.addComponent(comp, field.getLocation().getX(), field.getLocation().getY(), field.getLocation().getX() + field.getLocation().getWidth() - 1, field.getLocation().getY() + field.getLocation().getHeight() - 1);
    }
    
    public void initContainer(String property, Class clazz) {
        initContainer(property, clazz, null);
    }
    
    public void initContainer(String property, Class clazz, List values) {
        BeanItemContainer container = new BeanItemContainer(clazz);
        if (values != null)
            container.addAll(values);
        registerContainer(property, container);
    }
    
    protected void processField(SkorpioAbmField field, FieldGroup binder){}
    
    protected void bindAdditionalFields(FieldGroup binder) {}

    @Override
    protected void fixNullRepresentation() {
        grid.forEach((Component c) -> {
            if (c instanceof TextField)
                ((TextField)c).setNullRepresentation("");
            else if (c instanceof TextArea)
                ((TextArea)c).setNullRepresentation("");
        });
    }
    
    public void setRemoveItemHandler(RemoveItemHandler handler) {
        this.removeHandler = handler;
    }

    private void removeItem(BeanItem item) throws PersistenceException, SyncException {
        removeHandler.fireRemoveItem(item.getBean());
        itemRemoved(item);
    }
        
    public void setSaveItemHandler(SaveItemHandler handler) {
        saveHandler = handler;
    }

    private void saveItem(BeanItem item) throws PersistenceException, SyncException {
        String oldAction = action;
        saveHandler.fireSaveItem(action, item.getBean());
        itemSaved(oldAction, item);
    }
        
    public void addSavedItemListener(SavedItemListener listener) {
        if (savedListeners == null)
            savedListeners = new ArrayList<>();
        savedListeners.add(listener);
    }

    public void removeSavedItemListener(SavedItemListener listener) {
        if (savedListeners != null)
            savedListeners.remove(listener);
    }

    private void itemSaved(String action, BeanItem item) throws PersistenceException, SyncException {
        if (savedListeners != null) {
            for (SavedItemListener listener : savedListeners) {
                listener.fireItemSaved(action, item.getBean());
            }
        }
    }
        
    public void addEditItemListener(EditItemListener listener) {
        if (editListeners == null)
            editListeners = new ArrayList<>();
        editListeners.add(listener);
    }

    public void removeEditItemListener(EditItemListener listener) {
        if (editListeners != null)
            editListeners.remove(listener);
    }

    private void editItem() throws SyncException {
        if (editListeners != null) {
            for (EditItemListener listener : editListeners) {
                listener.fireEditItem(getBeanItem().getBean());
            }
        }
        updateUiPersistence(action, getBeanItem().getBean(), true);
    }
        
    public void addRemovedItemListener(RemovedItemListener listener) {
        if (removedListeners == null)
            removedListeners = new ArrayList<>();
        removedListeners.add(listener);
    }

    public void removeRemovedItemListener(RemovedItemListener listener) {
        if (removedListeners != null)
            removedListeners.remove(listener);
    }

    private void itemRemoved(BeanItem item) throws SyncException {
        if (removedListeners != null) {
            for (RemovedItemListener listener : removedListeners) {
                listener.fireItemRemoved(item.getBean());
            }
        }
    }

    private VerticalLayout buildMainLayout(int width, int height) {
        VerticalLayout mainLayout = new VerticalLayout();
        mainLayout.setImmediate(false);
        mainLayout.setWidth("100%");
        mainLayout.setHeight("100%");
        mainLayout.setMargin(false);

        // top-level component properties
        setWidth("100.0%");
        setHeight("100.0%");

        Panel panel_2 = buildPanel_2();
        mainLayout.addComponent(panel_2);
        
        lblInfo = new Label();
        lblInfo.setStyleName("locking");
        lblInfo.setSizeUndefined();
        lblInfo.setContentMode(ContentMode.HTML);
        mainLayout.addComponent(lblInfo);
        mainLayout.setComponentAlignment(lblInfo, Alignment.MIDDLE_CENTER);
        lblInfo.setVisible(false);
        
        Panel panel = new Panel();
        panel.setWidth("100.0%");
        panel.setHeightUndefined();
        //panel.addStyleName("transparent");
        panel.setContent(getContent(width, height));
        mainLayout.addComponent(panel);

        return mainLayout;
    }
    
    protected Component getContent(int width, int height) {
        grid = new GridLayout(width, height);
        grid.setMargin(true);
        grid.setSpacing(true);
        return grid;
    }
    
	private Panel buildPanel_2() {
            // common part: create layout
            Panel panel_2 = new Panel();
            panel_2.setImmediate(false);
            panel_2.setWidth("100.0%");
            panel_2.setHeight("80px");
            panel_2.addStyleName("transparent");

            // absoluteLayout_2
            AbsoluteLayout absoluteLayout_2 = buildAbsoluteLayout_2();
            panel_2.setContent(absoluteLayout_2);

            return panel_2;
	}

	private AbsoluteLayout buildAbsoluteLayout_2() {
            // common part: create layout
            AbsoluteLayout absoluteLayout_2 = new AbsoluteLayout();
            absoluteLayout_2.setImmediate(false);
            absoluteLayout_2.setWidth("100.0%");
            absoluteLayout_2.setHeight("100.0%");

            // embImage
//            embImage = new Image();
//            embImage.setImmediate(false);
//            embImage.setWidth("56px");
//            embImage.setHeight("56px");
//            embImage.setType(1);
//            embImage.setMimeType("image/png");
//            absoluteLayout_2.addComponent(embImage, "top:12.0px;left:12.0px;");

            // lblTitle
            lblTitle = new Label();
            lblTitle.setImmediate(false);
            lblTitle.setWidth("-1px");
            lblTitle.setHeight("-1px");
            lblTitle.setValue("<h2>Test</h2>");
            lblTitle.setContentMode(ContentMode.HTML);
//            absoluteLayout_2.addComponent(lblTitle, "top:14.0px;left:75.0px;");
            absoluteLayout_2.addComponent(lblTitle, "top:12.0px;left:12.0px;");

            // embDelete
            embDelete = new Embedded();
            embDelete.setImmediate(false);
            embDelete.setWidth("32px");
            embDelete.setHeight("32px");
            embDelete.setSource(new ThemeResource("remove.png"));
            embDelete.setType(1);
            embDelete.setMimeType("image/png");
            embDelete.setDescription("Eliminar");
            absoluteLayout_2.addComponent(embDelete, "right:12.0px;bottom:12.0px;");

            // embSave
            embSave = new Embedded();
            embSave.setImmediate(false);
            embSave.setWidth("32px");
            embSave.setHeight("32px");
            embSave.setSource(new ThemeResource("save.png"));
            embSave.setType(1);
            embSave.setMimeType("image/png");
            embSave.setDescription("Grabar");
            absoluteLayout_2.addComponent(embSave, "right:56.0px;bottom:12.0px;");

            // embEdit
            embEdit = new Embedded();
            embEdit.setImmediate(false);
            embEdit.setWidth("32px");
            embEdit.setHeight("32px");
            embEdit.setSource(new ThemeResource("edit.png"));
            embEdit.setType(1);
            embEdit.setMimeType("image/png");
            embEdit.setDescription("Editar");
            absoluteLayout_2.addComponent(embEdit, "right:100.0px;bottom:12.0px;");

            // embLock
            embLock = new Embedded();
            embLock.setImmediate(false);
            embLock.setWidth("32px");
            embLock.setHeight("32px");
            embLock.setSource(new ThemeResource("unlocked.png"));
            embLock.setType(1);
            embLock.setMimeType("image/png");
            embLock.setDescription("Unlocked");
            absoluteLayout_2.addComponent(embLock, "right:144.0px;bottom:12.0px;");

            return absoluteLayout_2;
	}

    @Override
    public Class getObjectClass() {
        return getBeanItem().getBean().getClass();
    }

    @Override
    public void objectLocked(SynchronizedObject obj) {
        if (isValidObject(obj)) 
            lockObject("Object locked by " + obj.getSystemUser().getUserName());
    }
    
    private boolean isValidObject(SynchronizedObject obj) {
        return (obj.getObjectId().equals(((SynchronizableObject)getBeanItem().getBean()).getId())) 
            && (!obj.getOwner().equals(getInstanceId()));
    }
    
    private void lockObject(String message) {
        embLock.setSource(new ThemeResource("locked.png"));
        embLock.setDescription(message);
        embEdit.setSource(new ThemeResource("edit_disabled.png"));
        embEdit.removeClickListener(editClickListener);
        embDelete.setSource(new ThemeResource("remove_disabled.png"));
        embDelete.removeClickListener(deleteClickListener);
        embSave.setSource(new ThemeResource("save_disabled.png"));
        embSave.removeClickListener(saveClickListener);
        lblInfo.setValue("<h1>" + message + "</h1>");
        lblInfo.setVisible(true);
        editing = false;
    }

    @Override
    public void objectReleased(SynchronizedObject obj) {
        if (isValidObject(obj)) {
            embLock.setSource(new ThemeResource("unlocked.png"));
            embLock.setDescription("Unlocked");
            disableEditing();
            lblInfo.setVisible(false);
            if (uiUpdateHandler != null)
                uiUpdateHandler.requestUiUpdate(obj.getObject());
        } else if (obj.getObjectId().equals(((SynchronizableObject)getBeanItem().getBean()).getId())) {
            if (obj.getForcedReleaseUser() != null)
                lockObject("Your object was released by " + obj.getForcedReleaseUser().toString());
        }
    }

    @Override
    public void objectRemoved(SynchronizedObject obj) {
        if (isValidObject(obj)) 
            lockObject("Object removed by " + obj.getSystemUser().getUserName());
    }

    @Override
    public void objectAdded(SynchronizedObject obj) {
    }
                
}
